﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 数据库路由表信息
    /// </summary>
    public class DataRouteAddModel
    {
         /// <summary>
         /// 学段
         /// </summary>
        [Required(ErrorMessage = "必填")]
        public string GradeID { set; get; }

        /// <summary>
        /// 课程
        /// </summary>

        [Required(ErrorMessage = "必填")]
        public string SubjectID { set; get; }

        /// <summary>
        /// 数据库名称
        /// </summary>

        [Required(ErrorMessage = "必填")]
        public string Database { set; get; }

        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        [Required(ErrorMessage="必填")]
        public string ConnString { set; get; }
    }
}