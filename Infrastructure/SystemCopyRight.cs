﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace EduPlatform.Manage.Infrastructure
{
    public class SystemCopyRight
    {
        public readonly string CompanyName;
        public SystemCopyRight()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(HttpContext.Current.Server.MapPath("/App_Data/copyright.config"));
            XmlNode compNode = xmlDoc.SelectSingleNode("/copyright/company");
            CompanyName = compNode.InnerText;
        }


    }
}