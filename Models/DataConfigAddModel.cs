﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 系统配置信息
    /// </summary>
    public class DataConfigAddModel
    {
        /// <summary>
        /// 配置键
        /// </summary>
        [Required(ErrorMessage="必填")]
        public string ConfigKey { set; get; }

        /// <summary>
        /// 配置值
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string ConfigValue { set; get; }

        /// <summary>
        /// 说明
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string ConfigNote { set; get; }
    }
}