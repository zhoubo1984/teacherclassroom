﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;

namespace TeacherClassRoom.Web.Logic
{
    public class BookInfoLogic
    {
        public BookSearchResponse GetBookInfo(BookSearchRequest request)
        {
            BookInfoSdk sdk = new BookInfoSdk();
            var rsp = sdk.GetBookInfo(request);

            return rsp;
        }
    }
}
