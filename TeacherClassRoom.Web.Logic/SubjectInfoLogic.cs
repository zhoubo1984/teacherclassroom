﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;
using TeacherClassRoomModel.RequestModel;

namespace TeacherClassRoom.Web.Logic
{
    public class SubjectInfoLogic
    {
        public SubjectInfoResponse GetSubjectInfo(SubjectGradeRequest request)
        {
            SubjectInfoSdk sdk = new SubjectInfoSdk();
            var rsp = sdk.GetSubjectInfo(request);

            return rsp;
        }

        public SubjectInfoResponse GetAllSubjectInfo()
        {
            SubjectInfoSdk sdk = new SubjectInfoSdk();
            var rsp = sdk.GetAllSubjectInfo();

            return rsp;
        }
    }
}
