﻿using EduPlatform.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Common.Log;

namespace TeacherClassRoom.Common
{
    public class CommonError
    {
        public static void CommonApplicationError(Exception ex)
        {
            LogFactory.CreateLog().LogError(ex);
        }

        public static void CommonApplicationError(BaseResponse response, Exception ex)
        {
            LogFactory.CreateLog().LogError(ex);
            if (response != null)
            {
                response.ResponseCode = ResponseCode.InernalError;
                response.ResponseText = "接口调用错误";
            }
        }

        public static void CommonApplicationError(BaseResponse response, ResponseCode code, Exception ex)
        {
            LogFactory.CreateLog().LogError(ex);
            if (response != null)
            {
                response.ResponseCode = ResponseCode.InernalError;
                response.ResponseText = "接口调用错误";
            }
        }
    }
}
