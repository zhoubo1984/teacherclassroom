﻿using EduPlatform.Logic.System;
using EduPlatform.SearchModel;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeacherClassRoom.VideoEditor.ViewModel;
using TeacherClassRoomLogic.Imp;
using TeacherClassRoomModel;
using TeacherClassRoomModel.ViewModel;

namespace TeacherClassRoom.VideoEditor
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private EduPlatform.Logic.System.GradeInfoLogic gradelogic = new EduPlatform.Logic.System.GradeInfoLogic();
        private TeacherClassRoomLogic.Imp.SubjectInfoLogic subjectlogic = new TeacherClassRoomLogic.Imp.SubjectInfoLogic();
        private TeacherClassRoomLogic.Imp.PressInfoLogic presslogic = new TeacherClassRoomLogic.Imp.PressInfoLogic();
        private EduPlatform.Logic.System.SubGradeInfoLogic subgradelogic = new EduPlatform.Logic.System.SubGradeInfoLogic();
        private TeacherClassRoomLogic.Imp.BookInfoLogic booklogic = new TeacherClassRoomLogic.Imp.BookInfoLogic();
        private TeacherClassRoomLogic.Imp.KnowledgeLogic knowledgelogic = new TeacherClassRoomLogic.Imp.KnowledgeLogic();
        private TeacherClassRoomLogic.Imp.VideoInfoLogic videologic = new VideoInfoLogic();
        private int currentPageIndex=1;
        private int pageSize = 10;

        Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadGrade();
        }

        private void BindingGrid(int pageindex,int pagesize)
        {
            TeacherClassRoomModel.RequestModel.VideoInfoListRequest request = new TeacherClassRoomModel.RequestModel.VideoInfoListRequest();
            request.GradeID = Guid.Parse(cmbGrade.SelectedValue.ToString());
            request.SubjectID = Guid.Parse(cmbSubject.SelectedValue.ToString());
            request.PressID = Guid.Parse(cmbPress.SelectedValue.ToString());
            request.BookID = Guid.Parse(cmbBook.SelectedValue.ToString());
            request.SubGradeID = Guid.Parse(cmbSubGrade.SelectedValue.ToString());
            request.KnowledgeID = new List<string>();
            foreach (var k in treeKnowledge.SelectedItems)
            {
                request.KnowledgeID.Add((k as TreeItemViewModel).NodeValue);
            }

            request.VideoName = txtVideoNameSearch.Text;
            request.PageIndex = pageindex;
            request.PageSize = pagesize;
            var pagelist = videologic.VideoInfoList(request);
            videogrid.ItemsSource = pagelist.Items;
            tbkTotal.Text = pagelist.TotalPages.ToString();
            tbkCurrentsize.Text = currentPageIndex.ToString();
        }

        private void LoadGrade()
        {
            try
            {
                //获取学段数据
                var gradelist = gradelogic.GetGradeInfoViewModels();
                cmbGrade.ItemsSource = gradelist;
                cmbGrade.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);  
            } 
        }

        private void BindKnowledgeTreeView(List<T_KnowledgeInfo> knowledgelist)
        {
            treeKnowledge.Items.Clear();
            var rootnodes = knowledgelist.Where(k => k.KG_PID == null);
            foreach (var r in rootnodes)
            {
                TreeItemViewModel tv = new TreeItemViewModel(null, false) { DisplayName = r.KG_Name, NodeValue = r.KG_ID.ToString() };
                treeKnowledge.Items.Add(tv);
                BindKnowledgeTreeItem(tv, knowledgelist);
            }
        }

        private void BindKnowledgeTreeItem(TreeItemViewModel pv, List<T_KnowledgeInfo> knowledgelist)
        {
            var childlist = knowledgelist.Where(k => k.KG_PID == Guid.Parse(pv.NodeValue)).ToList();
            foreach (var child in childlist)
            {
                TreeItemViewModel tvchild = new TreeItemViewModel(pv, false) { DisplayName = child.KG_Name, NodeValue = child.KG_ID.ToString() };
                pv.Children.Add(tvchild);
                BindKnowledgeTreeItem(tvchild, knowledgelist);
            }
        }

        private void cmbGrade_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var gradeid = Guid.Parse(cmbGrade.SelectedValue.ToString());
                var subjectlist = subjectlogic.SubjectInfoSearch(gradeid);
                var subgradelist = subgradelogic.GetSubGradeInfosByGradeID(gradeid);
                cmbSubject.ItemsSource = subjectlist;
                cmbSubject.SelectedIndex = 0;
                cmbSubGrade.ItemsSource = subgradelist;
                cmbSubGrade.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);  
            }
        }

        private void cmbSubject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if(cmbSubject.SelectedValue!=null)
                {
                    var subjectid = Guid.Parse(cmbSubject.SelectedValue.ToString());
                    var presslist = presslogic.GetPublishs(subjectid);
                    cmbPress.ItemsSource = presslist;
                    cmbPress.SelectedIndex = 0;

                    //设置知识点树
                    var knowledgelist = knowledgelogic.GetKnowledgesBySubjectId(subjectid).OrderBy(item=>item.KG_Index).ToList();

                    BindKnowledgeTreeView(knowledgelist);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);  
            }
        }

        private void cmbPress_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbPress.SelectedValue != null)
                {
                    var subjectid = Guid.Parse(cmbSubject.SelectedValue.ToString());
                    var pressid = Guid.Parse(cmbPress.SelectedValue.ToString());
                    var booklist = booklogic.GetBooks(subjectid, pressid);
                    cmbBook.ItemsSource = booklist;
                    cmbBook.SelectedIndex = 0;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);  
            }
        }

        private void ExpandMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (TreeItemViewModel node in treeKnowledge.SelectedItems)
            {
                node.IsExpanded = true;
            }
        }

        private void RenameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (TreeItemViewModel node in treeKnowledge.SelectedItems)
            {
                node.IsEditing = true;
                break;
            }
        }

        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (TreeItemViewModel node in treeKnowledge.SelectedItems.Cast<TreeItemViewModel>().ToArray())
            {
                node.Parent.Children.Remove(node);
            }
        }

        private void treeKnowledge_PreviewSelectionChanged(object sender, PreviewSelectionChangedEventArgs e)
        {
            
        }

        private void treeKnowledge_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void btnFileOpen_Click(object sender, RoutedEventArgs e)
        {

            if(treeKnowledge.SelectedItems.Count==0)
            {
                MessageBox.Show("请选择知识点", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                ofd.DefaultExt = ".mp4";
                ofd.Filter = "mp4 file|*.mp4";
                if (ofd.ShowDialog() == true)
                {
                    lblFilePath.Content = ofd.FileName;
                }
            }
            catch(Exception ex)
            {
                //日志操作
            }
            
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            CreateVideoInfoViewModel cv = new CreateVideoInfoViewModel();
            cv.SubGradeID = Guid.Parse(cmbSubGrade.SelectedValue.ToString());
            cv.GradeID = Guid.Parse(cmbGrade.SelectedValue.ToString());
            cv.SubjectID = Guid.Parse(cmbSubject.SelectedValue.ToString());
            cv.PressID = Guid.Parse(cmbPress.SelectedValue.ToString());
            cv.BookID = Guid.Parse(cmbBook.SelectedValue.ToString());
            cv.Author = txtAuthor.Text;
            cv.Description = txtDescription.Text;
            cv.FilePath = ofd.FileName;

            foreach (var k in treeKnowledge.SelectedItems)
            {
                cv.KnowledgeID += (k as TreeItemViewModel).NodeValue + "_";
            }
            cv.KnowledgeID = cv.KnowledgeID.TrimEnd('_');

            try
            {
                VideoInfoLogic videologic = new VideoInfoLogic();
                int result = videologic.AddNewVideo(cv);

                if (result > 0)
                {
                    MessageBox.Show("添加成功！", "消息提示");
                    BindingGrid(currentPageIndex, pageSize);
                }
                else
                {
                    MessageBox.Show("添加失败！", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(tbxPageNum.Text))
            {
                if (int.Parse(tbxPageNum.Text) > int.Parse(tbkTotal.Text) || int.Parse(tbxPageNum.Text)<1)
                {
                    MessageBox.Show("索引不合法", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    BindingGrid(int.Parse(tbxPageNum.Text), pageSize);
                }
            }
        }

        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            if (currentPageIndex <= 1)
            {
                MessageBox.Show("已到达当前最小页码", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            currentPageIndex--;
            BindingGrid(currentPageIndex, pageSize);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (currentPageIndex >= int.Parse(tbkTotal.Text))
            {
                MessageBox.Show("已到达当前最大页码", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            currentPageIndex++;
            BindingGrid(currentPageIndex, pageSize);
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            BindingGrid(currentPageIndex, pageSize);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(selectedVideoIds.Count==0)
                {
                    MessageBox.Show("请先选择要删除的视频", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if(MessageBox.Show("确认要进行视频删除吗？", "操作提示", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No)==MessageBoxResult.Yes)
                {
                    //1.进行数据库删除
                    int result = videologic.DeleteVideo(selectedVideoIds);
                    //2.进行物理文件删除
                    MessageBox.Show("删除成功！", "消息提示");
                    BindingGrid(currentPageIndex, pageSize);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("删除失败", "错误提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private List<Guid> selectedVideoIds = new List<Guid>();

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox dg = sender as CheckBox;
            Guid videoid = Guid.Parse(dg.Tag.ToString());   //获取该行的FID  
            var bl = dg.IsChecked;
            if (bl == true)
            {
                selectedVideoIds.Add(videoid);              //如果选中就保存FID  
            }
            else
            {
                selectedVideoIds.Remove(videoid);           //如果选中取消就删除里面的FID  
            }  
        }
    }
}
