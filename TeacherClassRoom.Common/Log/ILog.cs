﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Log
{
    public interface ILog
    {
        /// <summary>
        /// 普通信息日志
        /// </summary>
        /// <param name="message">普通警告信息</param>
        void LogInfo(string message);
        void LogInfo(Exception ex);
        void LogInfo(string message, Exception ex);

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="message">警告信息</param>
        void LogBug(string message);
        void LogBug(Exception ex);
        void LogBug(string message, Exception ex);

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message">错误信息</param>
        void LogError(string message);
        void LogError(Exception ex);
        void LogError(string message, Exception ex);
    }
}
