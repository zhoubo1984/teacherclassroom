﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeacherClassRoom.Web.Models
{
    public class AuthUserInfo
    {
        public Guid? UserId { get; set; }
    }
}