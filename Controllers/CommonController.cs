﻿using EduPlatform.Logic.System;
using EduPlatform.SearchModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduPlatform.Manage.Controllers
{
    public class CommonController : Controller
    {
        //
        // GET: /Common/
        [HttpPost]
        public JsonResult GetAreaInfo(int pid = 0)
        {
            AreaInfoLogic areainfoLogic = new AreaInfoLogic();
            var areas = areainfoLogic.GetAreas(pid);
            var areasModel = areas.Select(u => new AreaInfoModel() { AreaID = u.Area_ID, AreaName = u.Area_Name, PID = u.Area_PID });
            return Json(areasModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSubject(Guid gradeId)
        {
            SubjectInfoLogic subjectLogic = new SubjectInfoLogic();
            var subjects = subjectLogic.GetSubjects().Where(u=>u.Subject_GradeID==gradeId);
            var subjectModels = subjects.Select(u =>
            {
                return new SubjectInfoModel()
                {
                    GradeID = u.Subject_GradeID,
                    SubjectID = u.Subject_ID,
                    SubjectName = u.Subject_Name
                };
            });
            return Json(subjectModels, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetGrade()
        {
            GradeInfoLogic gradeLogic = new GradeInfoLogic();
            var grades = gradeLogic.GetGrades();
            var gradeModels = grades.Select(u => {
                return new GradeInfoModel() { 
                  GradeID = u.Grade_ID,GradeName = u.Grade_Name
                };
            });
            return Json(gradeModels,JsonRequestBehavior.AllowGet);
        }
    }
}
