﻿using EduPlatform.Framework;
using EduPlatform.Logic.System;
using EduPlatform.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EduPlatform.Manage.Controllers
{
    public class AccountController : BaseController
    {
        //
        // GET: /Account/
        AccountInfoLogic accountLogic = new AccountInfoLogic();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Home",null);
        }

        public new ActionResult Profile()
        {
            return View();
        }

        public ActionResult UpdatePwd()
        {
            return View();
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdatePwd(AccountUpdatePasswordModel model)
        {
            if(!ModelState.IsValid)
            {
                ViewBag.Msg = "输入错误";
                return View();
            }
            if(model.NewPassword!=model.ConfirmPassword)
            {
                ViewBag.Msg = "两次输入的密码不一致";
                return View();
            }
            var accountinfo = accountLogic.GetAccount(account.AccountName);
            if(Md5Helper.GetMd5Hash(model.OldPassword)!=accountinfo.Account_LoginPwd)
            {
                ViewBag.Msg = "原密码错误";
                return View();
            }
            accountinfo.Account_LoginPwd = Md5Helper.GetMd5Hash(model.NewPassword);
            bool updateResult=accountLogic.Update(accountinfo)>0;
            if(updateResult)
            {
                ViewBag.Msg = "修改密码成功";
            }
            return View();
        }
 
        [HttpPost]
        public ActionResult ResetAccountPwd(Guid uid)
        {
            UpdateResponse rsp = new UpdateResponse();
            try
            {
                SystemConfigLogic configLogic = new SystemConfigLogic();
                var accountinfo = accountLogic.GetModelByGuidId(uid);
                var configValue = configLogic.GetConfigByKey("userdefaultpwd").SC_ConfigValue;
                accountinfo.Account_LoginPwd = Md5Helper.GetMd5Hash(configValue);
                bool updateResult = accountLogic.Update(accountinfo) > 0;
                rsp.Data = configValue;
                rsp.Success = updateResult;
            }
            catch(Exception ex)
            {
                rsp.Data = "0";
                rsp.Success = false;
            }
            
            return Json(rsp);
        }
    }
}
