﻿using EduPlatform.Logic.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EduPlatform.Manage.Infrastructure
{
    /// <summary>
    /// 用户权限控制
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class AuthenUrlFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
            {
                SystemActionMenuLogic menuLogic = new SystemActionMenuLogic();
                AuthGroupMenuLogic groupAuthLogic = new AuthGroupMenuLogic();
                AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
                if (account.IsAdmin == 0)
                {
                    List<string> userMenus = groupAuthLogic.GetMeunsByUser(Guid.Parse(account.AccountId))
                       .Select(t => t.AuthMenu_MenuID).Distinct().ToList();
                    var converts = string.Join("|", userMenus).Split('|').Where(t => !string.IsNullOrEmpty(t))
                        .Select(t => string.Format("'{0}'", t)).ToList();
                    var userMenu = string.Join(",", converts);
                    List<string> actions = menuLogic.GetActionUrl(userMenu)
                        .Select(t => t.M_ActionUrl).ToList();
                    HttpRequestBase Request = filterContext.HttpContext.Request;
                    var isContains = actions.Where(t => t.Contains(Request.Url.LocalPath.ToLower())).ToList();
                    if (isContains.Count == 0)
                        filterContext.Result = new RedirectResult("/manage/index");
                }
            }
        }
    }
}