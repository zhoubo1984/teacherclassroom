﻿angular.module("dataServices", [])
    .constant("gradeListUrl", "/basalapi/gradelist")            //试卷条件查询
    .constant("subjectListUrl", "/basalapi/subjectlist")        //获取学段下的学科
    .constant("subjectAllListUrl", "/basalapi/subjectalllist")  //获取所有学科信息
    .constant("bookpressListUrl", "/basalapi/pressinfolist")
    .constant("bookListUrl", "/basalapi/bookinfolist")
    .constant("subgradeListUrl", "/basalapi/subjectallgradelist")
    .constant("knowledgeListUrl", "/basalapi/knowledgeinfolist")
    .constant("videoListUrl", "/basalapi/videoinfolist")
    .constant("videoCollectUrl", "/basalapi/CollectVideo")
    .constant("videoDeleteCollectUrl", "/basalapi/videocollectdelete")
    .constant("videoRecommendUrl", "/basalapi/RecommendVideo")
    .constant("videoDeleteRecommendUrl", "/basalapi/")
    .constant("collectListUrl", "/basalapi/videocollectedlist")
    .factory("commonService", function () {
        return {
            //阻止连接的默认行为
            stopEvent: function (event) {
                if (event) {
                    if (event.preventDefault) {
                        event.preventDefault();         //支持DOM标准的浏览器
                    } else {
                        event.returnValue = false;      //IE
                    }
                }
            }
        }
    })
    .factory("menuService", function () {
        
    })
    .factory("basalService", function ($http
        , gradeListUrl, subjectListUrl, subjectAllListUrl, bookpressListUrl, bookListUrl, subgradeListUrl
        , knowledgeListUrl, videoListUrl, videoCollectUrl, videoRecommendUrl, collectListUrl, videoDeleteCollectUrl) {
        return {
            userid: '',
            treeSetting: {
                view: {
                    showLine: false,
                },
                check: {
                    enable: true//,
                },
                data: {
                    simpleData: {
                        enable: true
                    },
                    key: {
                        title: "t"
                    }
                },
                callback: {
                    onCheck: function (event, treeId, treeNode) {

                    },
                    onClick: function (e, treeId, treeNode, clickFlag) {
                       
                    }
                }
            },
            getGradeList: function () {
                //获取所有年级信息
                return $http.post(gradeListUrl);
            },
            getSubjectList: function (gradeid) {
                //获取某年级下的学科信息
                return $http.post(subjectListUrl, { 'gradeid': gradeid });
            },
            getAllSubjectList: function () {
                return $http.post(subjectAllListUrl);
            },
            getBookPressList: function (subjectid) {
                return $http.post(bookpressListUrl, { 'subjectid': subjectid });
            },
            getSubGradeList: function () {
                return $http.post(subgradeListUrl);
            },
            getBookList: function (subjectid,pressid) {
                return $http.post(bookListUrl, { 'subjectid': subjectid, 'pressid': pressid });
            },
            getKnowledgeList: function (subjectid) {
                return $http.post(knowledgeListUrl, { 'subjectid': subjectid });
            },
            getVideoList: function (videoquery,pageindex,pagesize) {
                return $http.post(videoListUrl, {
                    'gradeid': videoquery.gradeinfo.GradeID,
                    'pressid': videoquery.bookpressinfo.PressID,
                    'bookid': videoquery.bookinfo.BookID,
                    'subgradeid': videoquery.subgradeinfo.SubGradeId,
                    'knowledgeid': videoquery.knowledgeids,
                    'subjectid': videoquery.subjectinfo.SubjectID,
                    'videoname': videoquery.videoname,
                    'orderby':videoquery.orderinfo.orderindex,
                    'pageindex': pageindex,
                    'pagesize':pagesize
                });
            },
            getCollectList:function(subjectcode,videoname,pageindex,pagesize){
                return $http.post(collectListUrl, {
                    'subjectcode': subjectcode,
                    'videoname': videoname,
                    'pageindex': pageindex,
                    'pagesize': pagesize
                });
            },
            collectVideo: function (videoid) {
                return $http.post(videoCollectUrl, { 'videoid': videoid });
            },
            deleteCollect: function (videoid) {
                return $http.post(videoDeleteCollectUrl, { 'videoid': videoid });
            },
            recommendVideo: function (videoid) {
                return $http.post(videoRecommendUrl, { 'videoid': videoid });
            },
            deleteRecommend:function(videoid){
                return $http.post(videoDeleteRecommendUrl, { 'videoid': videoid });
            },
            storeSearched:function(searchtext){
                searchtext = $.trim(searchtext);
                var list = JSON.parse(localStorage.getItem("searchedlist"));

                var isexists = false;
                for (var i = 0; i < list.length; i++) {
                    if (list[i] == searchtext) {
                        isexists = true;
                        break;
                    }
                }
                if (!isexists) {
                    list.unshift(searchtext);
                }

                localStorage.setItem("searchedlist", JSON.stringify(list));
            },
            storePlayed: function (video) {
                //{videoid,subjectname,playdate,videolengthinfo}
                
                var storekey = "playedlist_" + this.userid;
                var playedstr = localStorage.getItem(storekey);
                if (playedstr && playedstr.length > 0) {
                    var playedlist = JSON.parse(playedstr);
                    var isexists = false
                    for (var i = 0; i < playedlist.length; i++) {
                        if (playedlist[i].videoid == video.VideoID) {
                            playedlist[i].playdate = new Date();
                            isexists = true;
                        }
                    }
                    if (!isexists) {
                        playedlist.push({ 'videoid': video.VideoID, 'videoname': video.VideoName, 'playdate': new Date(), 'videolengthinfo': video.VideoLengthInfo, 'subjectname': video.SubjectName,'videopath':video.VideoPath });
                    }
                    playedstr = JSON.stringify(playedlist);
                }
                else {
                    var playedlist = [];
                    playedlist.push({ 'videoid': video.VideoID, 'videoname': video.VideoName, 'playdate': new Date(), 'videolengthinfo': video.VideoLengthInfo, 'subjectname': video.SubjectName, 'videopath': video.VideoPath });
                    playedstr = JSON.stringify(playedlist);
                }
                localStorage.setItem(storekey, playedstr);
                localStorage.setItem("currentVideo", JSON.stringify(video));
            },
            loadPlayedList: function () {
                var storekey = "playedlist_" + this.userid;
                var list = JSON.parse(localStorage.getItem(storekey));
                if (!list) {
                    list = [];
                    localStorage.setItem(storekey, JSON.stringify(list));
                }
                list.sort(function (a, b) {
                    return new Date(a.playdate) < new Date(b.playdate) ? 1 : -1;
                });

                return list;
            },
            loadSearchedList:function(){
                var list = JSON.parse(localStorage.getItem("searchedlist"));
                if (!list) {
                    list = [];
                    localStorage.setItem("searchedlist", JSON.stringify(list));
                }
                list.sort(function (a, b) {
                    return new Date(a.playdate) < new Date(b.playdate) ? 1 : -1;
                });

                return list;
            },

            loadPlayingVideo: function () {
                var playingvideo = JSON.parse(localStorage.getItem("currentVideo"));
                return playingvideo;
            },
            clearPlayedList: function () {
                localStorage.removeItem("playedlist_" + this.userid);
            },
            clearPlayedListWithout:function(videoid){
                var storekey = "playedlist_" + this.userid;
                var list = JSON.parse(localStorage.getItem(storekey));
                var savelist = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].videoid!= videoid) {
                        continue;
                    }
                    savelist.push(list[i]);
                }
                localStorage.setItem(storekey, JSON.stringify(savelist));
                return savelist;
            },
            clearSearchedList: function () {
                localStorage.removeItem("searchedlist");
            },
            removeSearchedItem: function (search) {
                var list = JSON.parse(localStorage.getItem("searchedlist"));
                var index = 0;
                for (var i = 0; i < list.length; i++) {
                    if (list[i] == search) {
                        index = i;
                        break;
                    }
                }
                list.splice(index, 1);
                localStorage.setItem("searchedlist", JSON.stringify(list));
                return list;
            },
            removePlayedItem: function (video) {
                var storekey = "playedlist_" + this.userid;
                var list = JSON.parse(localStorage.getItem(storekey));
                var index = 0;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].videoid == video.videoid) {
                        index = i;
                        break;
                    }
                }
                list.splice(index, 1);
                localStorage.setItem(storekey, JSON.stringify(list));
            },
            knowledgeQuery: function (subjectid) {
                return $http.post(knowledgeListUrl, { 'subjectid': subjectid });
            },
        }
    });
    