﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;
using EduPlatform.ServiceModel.Resource;

namespace TeacherClassRoom.Sdk
{
    public class KnowledgeInfoSdk:BaseSdk
    {
        public KnowledgeSearchResponse GetKnowledgeInfo(KnowledgeSearchRequest request)
        {
            CheckBaseUrl();
            var rsp = new KnowledgeSearchResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<KnowledgeSearchRequest, KnowledgeSearchResponse>(TCRSckFactory.BaseUrl, "api/konwledgeinfo/get", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }
    }
}
