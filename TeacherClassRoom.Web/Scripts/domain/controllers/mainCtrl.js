﻿//首页控制器
angular.module("tcApp").controller("mainCtrl", function ($scope, basalService, menuService) {

    var allsubject = [];    //存储所有的学科
    var alllsubgrade = [];  //存储所有的年级信息
    var knowledgedom = {};  //树形节点树
    var pagesize = 20;
    var userid = $("#hiduserid").val();
    basalService.userid = userid;
    $scope.pageindex = 1;
    $scope.conditionSign = -1;
    //默认项目
    $scope.gradelist = [{ 'GradeName': '全部', 'GradeCode': '', 'GradeID': '', 'Index': '-1' }];
    $scope.subjectlist = [{ 'SubjectName': '全部', 'SubjectCode': '', 'SubjectID': '', 'Index': '-1' }];
    $scope.bookpresslist = [{ 'PressName': '全部', 'PressNodeID': '', 'PressID': '' }];//
    $scope.booklist = [{ 'BookName': '全部', 'BookNodeID': '', 'BookID': '' }];//
    $scope.subgradelist = [{ 'SubGradeName': '全部', 'GradeId': '', 'SubGradeId': '', 'Index': '-1' }];
    $scope.knowledgelist = [];
    $scope.videolist = [];
    $scope.playedlist = [];
    $scope.searchedlist = [];
    $scope.orderinfo = [{ 'orderindex': 0, 'ordername': '上传时间' }, { 'orderindex': 1, 'ordername': '推荐次数' }, { 'orderindex': 2, 'ordername': '播放次数' }]
    $scope.orderby = 1;

    //加载播放列表
    $scope.initPlayedList = function () {

        $scope.playedlist.length = 0;
        var list = basalService.loadPlayedList();
        //删除3个月之外的数据
        for (var i = 0; i < list.length; i++) {
            if (((new Date()) - (new Date(list[i].playdate))) / (24 * 60 * 60 * 1000) <= 90) {
                $scope.playedlist.push(list[i]);
            }
        }
    };

    $scope.initSearchedList = function () {
        $scope.searchedlist.length = 0;
        $scope.videoquery.videoname = "";
        var list = basalService.loadSearchedList();
        //只保留10条记录
        for (var i = 0; i < list.length; i++) {
            $scope.searchedlist.push(list[i]);
        }
    };
    
    $scope.initKnowledgeTree = function () {

        function checkState(node) {
            node.checked = false;
            for (var i = 0; i < $scope.videoquery.knowledgeids.length; i++) {
                if ($scope.videoquery.knowledgeids[i] == node.id) {
                    node.checked = true;
                    break;
                }
            }
            if (node.children) {
                for (var j = 0; j < node.children.length; j++) {
                    checkState(node.children[j]);
                }
            }
            
        }

        for (var m = 0; m < $scope.knowledgelist.length; m++) {
            checkState($scope.knowledgelist[m]);
        }

        $.fn.zTree.init($('#booktree'), basalService.treeSetting, $scope.knowledgelist);
    };



    //查询条件
    $scope.videoquery = {
        gradeinfo: $scope.gradelist[0],
        subjectinfo: $scope.subjectlist[0],
        bookpressinfo: $scope.bookpresslist[0],
        bookinfo:$scope.booklist[0],
        subgradeinfo :$scope.subgradelist[0],
        knowledgeids: [],
        videoname: '',
        orderinfo: $scope.orderinfo[0]
    };

    $scope.changeCondition = function (sign) {
        //说明：0：学段选择 1：学科选择

        if (sign == 1||sign == 3) {
            if ($scope.videoquery.gradeinfo.Index == -1) {
                //layer.msg("请先选择学段", { icon: 5 });
                layer.alert("请先选择学段", { icon: 5 });
                return;
            }
        }
        else if (sign == 2||sign==5) {
            if ($scope.videoquery.subjectinfo.Index == -1) {
                layer.alert("请先选择学科", { icon: 5 });
                return;
            }
        }
        else if (sign == 4) {
            if ($scope.videoquery.bookpressinfo.PressID.length == 0) {
                layer.alert("请先选择教材版本", { icon: 5 });
                return;
            }
        }
        $scope.conditionSign = sign;
    };

    $scope.changeGrade = function (grade) {
        //更换学段
        $scope.videoquery.gradeinfo = grade;

        //更换学科
        var subarr = Enumerable.From(allsubject).Where(function (x) { return x.GradeID == grade.GradeID }).ToArray();
        subarr.unshift($scope.subjectlist[0]);
        $scope.subjectlist = subarr;
        //$scope.videoquery.subjectinfo = $scope.subjectlist[0];
        $scope.changeSubject($scope.subjectlist[0]);

        //更换年级
        var subgradearr = Enumerable.From(alllsubgrade).Where(function (x) { return x.GradeId == grade.GradeID }).ToArray();
        subgradearr.unshift($scope.subgradelist[0]);
        $scope.subgradelist = subgradearr;
        $scope.videoquery.subgradeinfo = $scope.subgradelist[0];

        //重新设置位置
        $scope.pageindex = 1;
        $scope.searchVideo();
    };

    $scope.changeSubject = function (subject) {
        //更换学科
        $scope.videoquery.subjectinfo = subject;
        if (subject.SubjectID.length == 0) {
            $scope.videoquery.bookpressinfo = { 'PressName': '全部', 'PressNodeID': '', 'PressID': '' };
            $scope.videoquery.bookinfo = $scope.booklist[0];
            $scope.videoquery.knowledgeids.length = 0;
            return;
        }
        //更换教材
        basalService.getBookPressList(subject.SubjectID).success(function (data) {
            if (data.ResponseCode == 1000) {
                $scope.bookpresslist = data.Press;
            }
            else {
                alert("教材信息加载失败");
            }
        }).error(function (a, b, c) {
            alert("教材信息服务异常，请重试。");
        });

        //获取该学科下的知识点
        basalService.knowledgeQuery(subject.SubjectID).success(function (data) {
            function getchecked() {
                var treeObj = $.fn.zTree.getZTreeObj("booktree");
                $scope.$apply(function () {
                    $scope.videoquery.knowledgeids = Enumerable.From(treeObj.getCheckedNodes(true)).Where(function (x) {
                        return (!x.children) || (x.children.length == 0);
                    }).Select(function (x) {
                        return x.id
                    }).ToArray();
                });
            }

            basalService.treeSetting.callback.onCheck = function (event, treeId, treeNode) {
                getchecked();
            };
            basalService.treeSetting.callback.onClick = function (e, treeId, treeNode, clickFlag) {
                var treeObj = $.fn.zTree.getZTreeObj("booktree");
                treeObj.checkNode(treeNode, !treeNode.checked, true);
                getchecked();
            };
            $scope.knowledgelist = data.Knowledges;
            $.fn.zTree.init($('#booktree'), basalService.treeSetting, $scope.knowledgelist);

        }).error(function (a, b, c) {
            layer.alert("知识点信息服务异常，请重试。");
        });

        $scope.pageindex = 1;
        $scope.searchVideo();
    };

    $scope.changeBookPress = function (press) {
        //更换出版社事件
        $scope.videoquery.bookpressinfo = press;
        var subjectid = $scope.videoquery.subjectinfo.SubjectID;

        //更换课本
        basalService.getBookList(subjectid,press.PressID).success(function (data) {
            if (data.ResponseCode == 1000) {
                data.Books.unshift($scope.booklist[0]);
                $scope.booklist = data.Books;
                $scope.videoquery.bookinfo = $scope.booklist[0];

                $scope.pageindex = 1;
                $scope.searchVideo();
            }
            else {
                alert("课本信息服务异常，请重试");
            }
        }).error(function (a, b, c) {
            alert("教材信息服务异常，请重试。");
        });
    };

    $scope.changeSubGrade = function (subgrade) {
        $scope.videoquery.subgradeinfo = subgrade;

        $scope.pageindex = 1;
        $scope.searchVideo();
    };

    $scope.changeBook = function (book) {
        $scope.videoquery.bookinfo = book;

        $scope.pageindex = 1;
        $scope.searchVideo();
    };

    $scope.changeKnowledge = function () {
        //选择知识点

        $scope.pageindex = 1;
        $scope.searchVideo();
        $scope.changeCondition(-1);
    };

    $scope.changeOrder = function (order) {
        $scope.videoquery.orderinfo = order;
        $scope.searchVideo();
    };

    $scope.shadowloaded = function () {
        $("li[sign]").removeClass("dian");
        $("li[sign='" + $scope.conditionSign + "']").addClass("dian");
    };

    $scope.collectVideo = function (video) {
        
        if (video.UserCollectCount == 0) {
            basalService.collectVideo(video.VideoID).success(function (data) {
                if (data.ResponseCode == 1000) {
                    video.UserCollectCount++;
                    layer.msg("收藏成功！", { icon: 1 });
                }
                else {
                    layer.msg(data.ResponseText, { icon: 5 });
                }
            }).error(function (a, b, c) {
                alert("收藏服务异常，请重试。");
            });
        }
        else {
            basalService.deleteCollect([video.VideoID]).success(function (data) {
                if (data.ResponseCode == 1000) {
                    video.UserCollectCount = 0;
                    layer.msg("取消成功！", { icon: 1 });
                }
                else {
                    layer.msg(data.ResponseText, { icon: 5 });
                }
            }).error(function (a, b, c) {
                alert("收藏服务异常，请重试。");
            });
        }
        
    };

    $scope.recommendVideo = function (video) {
        if (video.UserRecommend != 0) {
            return;
        }

        basalService.recommendVideo(video.VideoID).success(function (data) {

            if (data.ResponseCode == 1000) {
                video.UserRecommend++;
                video.RecommendCount++;
                layer.msg("推荐成功！", { icon: 1 });
            }
            else {
                layer.msg(data.ResponseText, { icon: 5 });
            }

        }).error(function (a, b, c) {
            alert("收藏服务异常，请重试。");
        });
    };

    $scope.playVideo = function (video) {

        basalService.storePlayed(video, userid);//先存储该视频到播放列表
        $scope.goplay(video.VideoID);
        //window.location.href = "/Home/Play?videopath=" + video.VideoPath;
    };

    $scope.goplay = function (videoid) {
        layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ['100%', '100%'],
            content: "/Home/Play?videoid=" + videoid //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }

    $scope.removePlayedItem = function (video) {
        basalService.removePlayedItem(video);
        var index = 0;
        for (var i = 0; i < $scope.playedlist.length; i++) {
            if ($scope.playedlist[i].videoid == video.videoid) {
                index = i;
                break;
            }
        }
        $scope.playedlist.splice(index, 1);
    }

    $scope.clearPlaylist = function () {
        basalService.clearPlayedList();
        $scope.playedlist.length = 0;

        layer.msg("清空完成", { icon: 1 });
    };

    $scope.clearSearched = function () {
        basalService.clearSearchedList();
        $scope.searchedlist.length = 0;
    }

    $scope.removeSearchedItem = function (search) {
        var list = basalService.removeSearchedItem(search);
        $scope.searchedlist = list;
    }

    $scope.manueSearch = function () {
        $scope.pageindex = 1;
        videoname = $.trim($scope.videoquery.videoname);
        if (videoname.length > 0) {
            basalService.storeSearched(videoname);
        }
        
        $scope.searchVideo();
        $scope.changeCondition(-1);
    }

    $scope.selectSearchItem = function (search) {
        $scope.videoquery.videoname = search;
    }

    //显示收藏
    $scope.showCollect = function () {

        layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ['100%', '100%'],
            content: ["/Home/MyCollect", 'no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
            cancel: function () {
                alert('!');
            }
        });
    };
   
    $scope.DeviceClose = function () {

        if (angular.isDefined(window.android)) {
            window.android.closeSelf();
        }
    };

    //获取学段信息
    basalService.getGradeList().success(function (data) {
        if (data.ResponseCode == 1000) {
            $scope.gradelist = $scope.gradelist.concat(data.Grades);
        }
        else {
            layer.msg("学段信息加载失败", {icon:5});
        }
    }).error(function (a, b, c) {
        layer.msg("学段信息服务异常，请重试。", { icon: 5 });
    });

    //获取所有的学科信息
    basalService.getAllSubjectList().success(function (data) {
        if (data.ResponseCode == 1000) {
            allsubject = data.Subjects;
        }
        else {
            layer.msg("学科信息服务异常，请重试。", { icon: 5 });
        }
    }).error(function (a, b, c) {
        layer.msg("学科信息服务异常，请重试。", { icon: 5 });
    });

    //获取所有的年级信息
    basalService.getSubGradeList().success(function(data){
        if (data.ResponseCode == 1000) {
            alllsubgrade =data.SubGradeInfos;
        }
        else {
            layer.msg("年级信息服务异常，请重试。", {icon:5});
        }
    }).error(function (a, b, c) {
        layer.msg("年级信息服务异常，请重试。", { icon: 5 });
    });

    
    
    //进行视频加载
    $scope.searchVideo=function() {
        var loadindex = layer.load();
        //加载视频列表
        return basalService.getVideoList($scope.videoquery, $scope.pageindex, pagesize).success(function (data) {
            if (data.ResponseCode == 1000) {
                if ($scope.pageindex == 1)
                    $scope.videolist = data.Videos;
                else
                    $scope.videolist = $scope.videolist.concat(data.Videos);
            }
            else {
                layer.msg("视频信息服务异常，请重试。", { icon: 5 });
            }
        }).error(function (a, b, c) {
            layer.msg("收藏信息服务异常，请重试。", { icon: 5 });
        }).then(function () {
            layer.close(loadindex);
        });
    }

    $scope.searchPress = function (event) {
        var key_code = event.keyCode;
        if (key_code == 13) {
            $scope.manueSearch();
        }
    }
    //加载视频列表
    $scope.searchVideo();
});

