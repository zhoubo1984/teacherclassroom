﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Log
{
    public interface ILogFactory
    {
        ILog Create();
    }
}
