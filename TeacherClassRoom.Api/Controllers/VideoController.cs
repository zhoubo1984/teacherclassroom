﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeacherClassRoom.Common;
using TeacherClassRoomLogic.Imp;
using TeacherClassRoomModel.RequestModel;
using TeacherClassRoomModel.ResponseModel;

namespace TeacherClassRoom.Api.Controllers
{
    public class VideoController : ApiController
    {
        /// <summary>
        /// 获取视频信息列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoInfoListResponse VideoInfoList(VideoInfoListRequest model)
        {
            var response = new VideoInfoListResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var result = logic.VideoInfoList(model);
                response.TotalCount = result.TotalItems;
                response.TotalPage = result.TotalPages;
                response.ResponseText = "获取成功";
                response.PageSize = result.ItemsPerPage;
                response.Videos = result.Items;
                response.DataCount = result.Items.Count;
                response.PageIndex = result.CurrentPage;
                response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
            }
            catch (Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }

        /// <summary>
        /// 获取单个视频信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoResponse VideoInfo(VideoInfoRequest model)
        {
            var response = new VideoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var result = logic.VideoInfo(model);

                if(result!=null)
                {
                    response.DataCount = 1;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                    response.Video = result;
                    response.ResponseText = "获取查询";
                }
                else
                {
                    response.DataCount = 0;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                    response.ResponseText = "为找到该视频";
                }
            }
            catch(Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }

        /// <summary>
        /// 视频收藏列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoInfoListResponse VideoCollectedList(VideoCollectListRequest model)
        {
            var response = new VideoInfoListResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var result = logic.VideoCollectList(model);
                response.TotalCount = result.TotalItems;
                response.TotalPage = result.TotalPages;
                response.ResponseText = "获取成功";
                response.PageSize = result.ItemsPerPage;
                response.Videos = result.Items;
                response.DataCount = result.Items.Count;
                response.PageIndex = result.CurrentPage;
                response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
            }
            catch (Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }

        /// <summary>
        /// 学科下的收藏的视频数量统计
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public SubjectVideoCountResponse SubjectVideoCount(SubjectVideoCountRequest request)
        {
            var response = new SubjectVideoCountResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var resultlist = logic.SubjectVideoCount(request);
                response.DataCount = resultlist.Count;
                response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                response.ResponseText = "获取成功！";
                response.SubjectVideoCount = resultlist;
            }
            catch(Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }

        /// <summary>
        /// 视频推荐接口
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoInfoResponse Recommend(VideoRecommendRequest request)
        {
            var response = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                int result = logic.VideoRecommend(request.VideoID, request.UserID);
                response.DataCount = 1;
                response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                response.ResponseText = result==1?"推荐成功！":"取消推荐成功！";
            }
            catch(Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }

        /// <summary>
        /// 视频收藏删除接口
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoInfoResponse VideoCollectDelete(VideoCollectDeleteRequest request)
        {
            var response = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var collectvideo = logic.VideoCollectDelete(request.VideoID, request.UserID);
                if (collectvideo >0)
                {
                    response.ResponseText = "删除成功！";
                    response.DataCount = collectvideo;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                    return response;
                }
                else
                {
                    response.ResponseText = "删除失败！";
                    response.DataCount = 0;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                }
            }
            catch (Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }
            return response;
        }

        /// <summary>
        /// 收藏视频
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public VideoInfoResponse Collect(VideoCollectRequest request)
        {
            var response = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                var collectvideo = logic.GetUserCollect(request.VideoID, request.UserID);
                if (collectvideo != null)
                {
                    response.ResponseText = "该视频不得重复收藏！";
                    response.DataCount = 0;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.DataExist;

                    return response;
                }
                int result = logic.Collect(request.VideoID, request.UserID,request.Description);
                if (result > 0)
                {
                    response.ResponseText = "收藏成功！";
                    response.DataCount = 1;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                }
                else
                {
                    response.ResponseText = "收藏失败！";
                    response.DataCount = 0;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                }
            }
            catch(Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }
            

            return response;
        }

        [HttpPost]
        public VideoInfoResponse VideoUpdatePlayCount(VideoUpdatePlayCountRequest request)
        {
            var response = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                int result = logic.UpdatePlayCountVideo(request.VideoID.Value);
                if (result > 0)
                {
                    response.ResponseText = "更新成功！";
                    response.DataCount = result;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Success;
                    return response;
                }
                else
                {
                    response.ResponseText = "更新失败！";
                    response.DataCount = 0;
                    response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                }
            }
            catch(Exception ex)
            {
                CommonError.CommonApplicationError(response, ex);
            }

            return response;
        }
    }
}
