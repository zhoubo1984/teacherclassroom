﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common
{
    public class FileManager
    {
        public string AddTempFile(string filedir,byte[] filecontent)
        {

            if(!Directory.Exists(filedir))
            {
                Directory.CreateDirectory(filedir);
            }

            string filepath = Path.Combine(filedir, Guid.NewGuid().ToString() + ".tmp");
            using (FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
            {
                fs.Write(filecontent, 0, filecontent.Count());
            }

            return filepath;
        }
    }
}
