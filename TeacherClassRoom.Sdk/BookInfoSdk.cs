﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;

namespace TeacherClassRoom.Sdk
{
    public class BookInfoSdk:BaseSdk
    {
        public BookSearchResponse GetBookInfo(BookSearchRequest request)
        {
            CheckBaseUrl();
            var rsp = new BookSearchResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<BookSearchRequest, BookSearchResponse>(TCRSckFactory.BaseUrl, "api/bookinfo/get", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }
    }
}
