﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeacherClassRoom.Web.Helpers
{
    public class SubjectCodeToName
    {
        public static string Convert(string subjectcode)
        {
            string name=string.Empty;
            switch(subjectcode)
                {
                    case "English":
                        name = "英语";
                        break;
                    case "Math":
                        name = "数学";
                        break;
                    case "Chinese":
                        name = "语文";
                        break;

                    case "Biology":
                        name = "生物";
                        break;
                    case "Chemistry":
                        name = "化学";
                        break;
                    case "Geography":
                        name = "地理";
                        break;
                    case "History":
                        name = "历史";
                        break;
                    case "Physics":
                        name = "物理";
                        break;
                    case "Politics":
                        name = "政治";
                        break;
                    default:
                        name = "其它";
                        break;
                }
            return name;
        }
    }
}