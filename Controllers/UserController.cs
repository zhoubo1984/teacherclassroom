﻿using EduPlatform.Domain.Teacher;
using EduPlatform.Framework;
using EduPlatform.Logic.System;
using EduPlatform.Logic.User;
using EduPlatform.Manage.Infrastructure;
using EduPlatform.Manage.Models;
using EduPlatform.SearchModel;
using EduPlatform.SearchModel.User;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using EduPlatform.Domain.System;
using EduPlatform.Logic.Teacher;
using EduPlatform.ServiceModel;
using EduPlatform.ServiceModel.Module;
using EduPlatform.ServiceModel.Module.Teacher;
using GradeSubjectViewModel = EduPlatform.Manage.Models.GradeSubjectViewModel;
using TeacherClassRoomModel.ViewModel;
using TeacherClassRoomLogic.Imp;
using TeacherClassRoom.Common;
using System.IO;
using System.Text.RegularExpressions;
using TeacherClassRoomModel;

namespace EduPlatform.Manage.Controllers
{
    /// <summary>
    /// 用户管理
    /// </summary>
    public class UserController : BaseController
    {
        //
        // GET: /User/
        StudentInfoLogic studentLogic = new StudentInfoLogic();
        TeacherLogic teacherInfoLogic = new TeacherLogic();
        TeacherInfoLogic teacherLogic = new TeacherInfoLogic();
        UserInfoLogic userLogic = new UserInfoLogic();
        SignContractLogic signContractLogic = new SignContractLogic();
        SystemConfigLogic configLogic = new SystemConfigLogic();
        EduPlatform.Logic.System.GradeInfoLogic gradeLogic = new EduPlatform.Logic.System.GradeInfoLogic();

        UserLogic classroomuserLogic = new UserLogic();

        public ActionResult Index()
        {
            return View();
        }
        [AuthenUrlFilter]
        public ActionResult StudentList()
        {
            AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
            var classroomunitLogic = new UnitLogic();
            var unitid = classroomunitLogic.GetModelByAccountId(Guid.Parse(account.AccountId));
            if (unitid.Count > 0)
            {
                ViewBag.puid = unitid[0].Unit_ID;
                ViewBag.puname = unitid[0].Unit_Name;
            }
            var grades = gradeLogic.GetGrades();
            return View(grades);
        }

        //学生用户列表
        [HttpPost]
        public ActionResult StudentResultList(StudentSearchViewModel search)
        {
            AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
            search.AdminUid = Guid.Parse(account.AccountId);
            ViewBag.CurrentUrl = "user/studentlist";
            var students = classroomuserLogic.StudentInfoSearch(search);
           
            return View(students);
        }

        //批量导入学生
        [HttpPost]
        public ActionResult ImportStudent(StudentImportViewModel model, HttpPostedFileBase file)
        {
            var response = new BaseResponse();
            if (ModelState.IsValid&&file != null)
            {
                //检查文件
                if (file.ContentLength > 4 * 1024 * 1024)
                {
                    
                    response.DataCount = 0;
                    response.ResponseCode = ResponseCode.ParamError;
                    response.ResponseText = "文件上传不得大于4M";

                    return Json(response);
                }
                //保存数据
                byte[] filecontent = file.InputStream.ToByteArray();
                FileManager fm = new FileManager();
                string tmpfilepath = fm.AddTempFile(Server.MapPath("~/TempFiles"),filecontent);
                classroomuserLogic.ImportStudent(model, tmpfilepath);

                response.DataCount = 1;
                response.ResponseCode = ResponseCode.Success;
                response.ResponseText = "导入成功！";

                return Json(response);
            }
            else
            {
                response.DataCount = 0;
                response.ResponseCode = ResponseCode.ParamError;
                response.ResponseText = "请求错误，请检查输入";

                return Json(response);
            }
        }

        
        /// <summary>
        /// 导出学生
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExportStudent()
        {
            AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
            Guid? uid = null;
            if (account.IsAdmin != 1) uid = Guid.Parse(account.AccountId);
            MemoryStream stream = classroomuserLogic.ExportStudent(uid);
            byte[] content = stream.ToArray();
            return File(content, "application/vnd.ms-excel", Server.UrlEncode("用户下载.xls"));
        }

        //教师列表
        //[AuthenUrlFilter]
        public ActionResult TeacherList()
        {
            return View();
        }


        [HttpPost]
        public ActionResult TeacherResultList(TeacherSearchModel search)
        {
            ViewBag.CurrentUrl = "user/teacherlist";
            var teachers = teacherInfoLogic.TeacherInfoSearch(search);
            return View(teachers);
        }

       /// <summary>
       /// 启用用户
       /// </summary>
       /// <param name="uid"></param>
       /// <param name="state"></param>
       /// <returns></returns>
        [HttpPost]
        public JsonResult EnableUser(Guid uid,int state)
        {
            var rsp = new BaseResponse();
            var user = classroomuserLogic.GetModelByGuidId(uid);
            user.User_State = state;
            int count =classroomuserLogic.Update(user);

            if(count>0)
            {
                rsp.DataCount = 1;
                rsp.ResponseCode = ResponseCode.Success;
                rsp.ResponseText = "操作成功！";
                return Json(rsp);
            }

            rsp.DataCount = 0;
            rsp.ResponseCode = ResponseCode.InernalError;
            rsp.ResponseText = "操作失败！";
            return Json(rsp);
        }

        [HttpPost]
        public JsonResult ActiveTeacher(Guid id)
        {
            var rsp = new UpdateResponse();
            TeacherLogic logic = new TeacherLogic();
            try
            {
                int result = logic.ActiveTeacher(id);

                if(result>0)
                {
                    rsp.Success = true;
                    rsp.Message = "删除成功！";
                }
                else
                {
                    rsp.Success = false;
                    rsp.Message = "删除失败！";
                }
                
            }
            catch(Exception ex)
            {
                rsp.Success = false;
                rsp.Message = "服务器错误";
            }

            return Json(rsp);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteUser(Guid uid)
        {
            var rsp = new UpdateResponse();
            bool updateResult = false;

            var user = classroomuserLogic.GetModelByGuidId(uid);

            if (user.User_Type == (int)UserType.Teacher)
            {
                // updateResult = userInfoLogic.TeacherDelete(uid);
                // 20170306 删除教师信息
                // 由逻辑删除修改为物理删除

                TeacherInfoLogic teacherInfoLogic = new TeacherInfoLogic();
                updateResult=teacherInfoLogic.ClearTeacherInfo(uid);
            }
            else if(user.User_Type == (int)UserType.Student)
            {
                StudentInfoLogic studentInfoLogic = new StudentInfoLogic();
                updateResult = studentInfoLogic.ClearStudentInfo(uid);
                //updateResult = userInfoLogic.StudentDelete(uid);
            }
            else
            {
                updateResult = false;
            }
            rsp.Success = updateResult;
            rsp.Message = "请求处理成功";
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        //添加用户
        public ActionResult UserAdd()
        {
            var grades = gradeLogic.GetGrades();
            AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
            var classroomunitLogic = new UnitLogic();
            var unitid = classroomunitLogic.GetModelByAccountId(Guid.Parse(account.AccountId));
            if(unitid.Count>0)
            {
                ViewBag.puid = unitid[0].Unit_ID;
                ViewBag.puname = unitid[0].Unit_Name;
            }
           
            return View(grades);
        }

        //添加用户
        [HttpPost]
        public ActionResult UserAdd(StudentAddViewModel model)
        {
            var configValue = configLogic.GetConfigByKey("userdefaultpwd").SC_ConfigValue;
            var grades = gradeLogic.GetGrades();
            if(!ModelState.IsValid)
            {                
                return View(grades);
            }
            if (model.StartDate > model.EndDate)
            {
                ModelState.AddModelError("EndDate", "结束时间不能大于起始时间");
                return View(grades);
            }

            model.UserPwd = Md5Helper.GetMd5Hash(configValue);
            model.UserId = Guid.NewGuid();

            classroomuserLogic.AddStudent(model);
            return RedirectToAction("studentlist", "user", new { model.UserId });
        }

        public ActionResult TeacherEdit(Guid tid)
        {
            

            TeacherLogic teacherlogic = new TeacherLogic();
            var teacher = teacherlogic.GetModelByGuidId(tid);

            TeacherClassRoomLogic.Imp.GradeInfoLogic gradelogic = new TeacherClassRoomLogic.Imp.GradeInfoLogic();
            var gradelist = gradelogic.Fetch();
            var gradedroplist = new SelectList(gradelist, "Grade_ID", "Grade_Name", teacher.Teacher_GradeID);

            TeacherClassRoomLogic.Imp.SubjectInfoLogic subjectlogic = new TeacherClassRoomLogic.Imp.SubjectInfoLogic();
            var subjectlist = Newtonsoft.Json.JsonConvert.SerializeObject(subjectlogic.Fetch());

            ViewBag.GradeDropList = gradedroplist;
            ViewBag.SubjectList = subjectlist;
            ViewBag.TeacherSubjectID = teacher.Teacher_SubjectID.ToString();
            return View(teacher);
        }


        [HttpPost]
        public ActionResult TeacherEdit(T_Teacher teacher)
        {
            TeacherLogic teacherlogic = new TeacherLogic();
            int result = teacherlogic.Update(teacher);

            return RedirectToAction("TeacherList");
        }

        /// <summary>
        /// 编辑用户信息
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public ActionResult UserEdit(Guid uid)
        {
            //T_UserInfo userinfo = userLogic.GetModelByGuidId(uid);

            TeacherClassRoomModel.T_UserInfo userinfo = classroomuserLogic.GetModelByGuidId(uid);

            var teacherSubjectLogic = new TeacherSubjectLogic();
            var subjectlist = teacherSubjectLogic.TeacherSubjectInfoGet(uid);
            ViewBag.UserType = userinfo.User_Type;
            ViewBag.SubjectInfos = subjectlist;
            ViewBag.UserName = userinfo.User_LoginName;
            ViewBag.UserId = uid.ToString();
            
            ViewBag.UserTrueName = userinfo.User_TrueName;
            ViewBag.UserEmail = userinfo.User_Email;

            var signContract = signContractLogic.GetSignContractByUid(uid);
            SignContractInfo signContractInfo = new SignContractInfo()
            {
                EndDate = signContract.SC_EndDate,
                StartDate = signContract.SC_StartDate,
                Uid = signContract.SC_UserID,
            };
            return View(signContractInfo);
        }

        [HttpPost]
        public ActionResult UserEdit(SignContractInfo model, FormCollection collection, int usertype = 0)
        {
            var signContract = signContractLogic.GetSignContractByUid(model.Uid);

            signContract.SC_EndDate = model.EndDate;
            signContractLogic.Update(signContract);

            //var userInfo = classroomuserLogic.GetModelByGuidId(model.Uid);

            ViewBag.UserType = usertype;

            var usertruename = collection["UserTrueName"];
            var useremail = collection["UserEmail"];

            var userinfo = classroomuserLogic.GetModelByGuidId(model.Uid);
            userinfo.User_Email = useremail;
            userinfo.User_TrueName = usertruename;
            var updateuser = classroomuserLogic.Update(userinfo);

            ////如果是教师的话
            //if (usertype == 1)
            //{
            //    var teacherSubjectLogic = new TeacherSubjectLogic();
            //    var subjectinfolist = teacherSubjectLogic.TeacherSubjectInfoGet(model.Uid);

            //    if (collection.GetValues("chkAdmin") != null)
            //    {
            //        string checksubjectinfo = collection.GetValue("chkAdmin").AttemptedValue;
            //        string[] subjectinfoids = checksubjectinfo.Split(',');
            //        foreach (string r in subjectinfoids)
            //        {
            //            if (subjectinfolist.Where(item => item.SubjectId.ToString() == r).FirstOrDefault().IsAdmin == 0)
            //            {
            //                var ts = teacherSubjectLogic.GetTeacherSubject(model.Uid, Guid.Parse(r), (Guid)userInfo.User_UnitID);
            //                ts.TS_IsAdmin = 1;
            //                teacherSubjectLogic.Update(ts);
            //            }
            //        }

            //        foreach (var notchose in subjectinfolist.Where(item => !subjectinfoids.Contains(item.SubjectId.ToString())))
            //        {
            //            if (notchose.IsAdmin != 0)
            //            {
            //                var ts = teacherSubjectLogic.GetTeacherSubject(model.Uid, notchose.SubjectId, (Guid)userInfo.User_UnitID);
            //                ts.TS_IsAdmin = 0;
            //                teacherSubjectLogic.Update(ts);
            //            }
            //        }
            //    }
            //    return RedirectToAction("teacherlist", "user", new { uid = model.Uid });
            //}

            return RedirectToAction("studentlist", "user", new { uid = model.Uid });
        }


        /// <summary>
        /// 重置用户密码
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ResetUserPwd(Guid uid)
        {
            UpdateResponse rsp = new UpdateResponse();
            try
            {
                var user = classroomuserLogic.GetModelByGuidId(uid);
                var configValue = configLogic.GetConfigByKey("userdefaultpwd").SC_ConfigValue;
                user.User_LoginPwd = Md5Helper.GetMd5Hash(configValue);
                bool updateResult = classroomuserLogic.Update(user) > 0;
                rsp.Data = configValue;
                rsp.Success = updateResult;
            }
            catch(Exception ex)
            {
                rsp.Data = "0";
                rsp.Success = false;
            }
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VerifyUserList(string uids)
        {
            var rsp = new BaseResponse();
            if (string.IsNullOrEmpty(uids))
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = ResponseCode.ParamError;
                rsp.ResponseText = "请求参数错误";

                return Json(rsp);
            }

            try
            {
                classroomuserLogic.VerifyUserList(uids.Split('|'));
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = ResponseCode.InernalError;
                rsp.ResponseText = "批量审批失败！";

                return Json(rsp);
            }

            rsp.DataCount = 1;
            rsp.ResponseCode = ResponseCode.Success;
            rsp.ResponseText = "审核成功！";

            return Json(rsp);
        }

        /// <summary>
        /// 用户注册审核
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult VerifyUser(Guid uid)
        {
            UpdateResponse rsp = new UpdateResponse();
            var user = classroomuserLogic.GetModelByGuidId(uid);
            if (user == null)
            {
                rsp.Success = false;
                rsp.Message = "用户信息不存在";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            if (user.User_CheckState != 0)
            {
                rsp.Success = false;
                rsp.Message = "已审核";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            user.User_CheckState = 1;
            int updateResult = classroomuserLogic.Update(user);
            rsp.Success = updateResult > 0;
            rsp.Message = rsp.Success ? "审核成功" : "审核失败";
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TeacherSubjectEdit(Guid uid)
        {
            ViewBag.CurrentUrl = "user/teachersubjectedit";
            UserInfoLogic userInfoLogic = new UserInfoLogic();
            var userInfo = userInfoLogic.GetModelByGuidId(uid);
            ViewBag.TrueName = userInfo.User_TrueName;
            ViewBag.Uid = uid;
            return View();
        }

        [HttpPost]
        public void GetSubjects(Guid uid, Guid? unitId = null)
        {
            TeacherInfoLogic teacherInfoLogic = new TeacherInfoLogic();
            var teacherInfo = teacherInfoLogic.GetModelByGuidId(uid);
            UnitInfoLogic unitInfoLogic = new UnitInfoLogic();
            var unitInfo = unitInfoLogic.GetModelByGuidId(teacherInfo.Teacher_UnitID);

            var subjectLogicCache = new Logic.System.SubjectInfoLogic();
            var subjects = subjectLogicCache.GetGradeSubjectViewModels();

            EduPlatform.Logic.System.GradeInfoLogic gradeInfoLogic = new EduPlatform.Logic.System.GradeInfoLogic();
            var grades = gradeInfoLogic.GetGradeInfoViewModels();

            TeacherSubjectLogic teacherSubjectLogic = new TeacherSubjectLogic();
            var teacherSubjects = teacherSubjectLogic.TeacherSubjectInfoGet(uid);

            int gId = 1;

            
            List<Guid> unitGradeIds = null;
            if (unitId != null)
            {
                EduPlatform.Domain.System.T_UnitInfo unit = unitInfoLogic.GetModelByGuidId((Guid)unitId);
                unitGradeIds = unit.Unit_Grade.Split(',').Select(u =>
                {
                    return Guid.Parse(u);
                }).ToList();
            }

            unitGradeIds = unitInfo.Unit_Grade.Split(',').Select(u =>
            {
                return Guid.Parse(u);
            }).ToList();

            List<GradeInfoViewModel> unitGrades = new List<GradeInfoViewModel>();

            if (!unitGradeIds.IsHasData())
            {
                unitGradeIds = new List<Guid>();
                unitGrades = grades;
            }
            else
            {
                unitGrades = grades.Where(u =>
                {
                    return unitGradeIds.Exists(g => g == u.GradeID);
                }).ToList();
            }

            List<GradeSubjectViewModel> items = new List<GradeSubjectViewModel>();
            GradeSubjectViewModel item;
            int index = 0;
            foreach (var g in unitGrades)
            {

                item = new GradeSubjectViewModel()
                {
                    id = gId,
                    name = g.GradeName,
                    nocheck = true,
                    open = false,
                    pId = 0,
                    code = g.GradeCode.ToString()
                };


                items.Add(item);
                gId++;

                subjects.Where(u => u.GradeID == g.GradeID).ToList().ForEach(u =>
                {
                    items.Add(new GradeSubjectViewModel()
                    {
                        id = gId,
                        name = u.SubjectName,
                        code = u.SubjectID.ToString().ToLower(),
                        nocheck = false,
                        open = false,
                        pId = item.id
                    });

                    gId++;
                });

            }

            StringBuilder js = new StringBuilder("[");
            bool isExist = false;
            TeacherSubjectInfoModel ts;
            for (int i = 0; i < items.Count; i++)
            {
                ts = teacherSubjects.SingleOrDefault(u => u.SubjectId.ToString().ToLower() == items[i].code.ToLower());
                if (ts != null)
                {
                    isExist = true;
                }
                else
                {
                    isExist = false;
                }
                js.Append("{\"id\":" + items[i].id + ",\"pId\":" + items[i].pId + ",\"name\":\"" + items[i].name + "\",\"open\":" + false.ToString().ToLower() + ",\"nocheck\":" + false.ToString().ToLower() + ",\"code\":\"" + items[i].code + "\",\"checked\":" + isExist.ToString().ToLower() + "}");
                if (i != items.Count - 1)
                {
                    js.Append(",");
                }

            }
            js.Append("]");
            Response.Write(js.ToString());
        }

        [HttpPost]
        public JsonResult TeacherSubjectEdit(string subjects, Guid uid)
        {
            UpdateResponse rsp = new UpdateResponse();

            if (string.IsNullOrEmpty(subjects))
            {
                rsp.Success = false;
                rsp.Message = "至少要保留一个课程!";

                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            UserInfoLogic userInfoLogic = new UserInfoLogic();
            UnitInfoLogic unitInfoLogic = new UnitInfoLogic();
            var userInfo = userInfoLogic.GetModelByGuidId(uid);
            var unitInfo = unitInfoLogic.GetModelByGuidId((Guid)userInfo.User_UnitID);


            var upIds = subjects.Split('|').Select(u => Guid.Parse(u)).ToList();
            TeacherSubjectLogic teacherSubjectLogic = new TeacherSubjectLogic();
            var teacherSubjects = teacherSubjectLogic.TeacherSubjectInfoGet(uid);
            var tsIds = teacherSubjects.Select(u => u.SubjectId).ToList();

            // 交集
            var existIds = upIds.Intersect(tsIds).ToList();

            // 
            var delIds = tsIds.Except(upIds).ToList();
            var newIds = upIds.Except(tsIds).ToList();

            int leftCount = tsIds.Count - delIds.Count + newIds.Count;
            if (leftCount <= 0)
            {
                rsp.Success = false;
                rsp.Message = "至少要保留一个课程!";
                
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            var newTs = newIds.Select(u =>
            {
                return new T_TeacherSubject()
                {
                    TS_AddDate = DateTime.Now,
                    TS_ID = Guid.NewGuid(),
                    TS_IsAdmin = 0,
                    TS_State = 1,
                    TS_SubjectID = u,
                    TS_TeacherID = uid,
                    TS_UnitID = unitInfo.Unit_ID
                };
            }).ToList();

            bool updateResult = teacherSubjectLogic.UpdateTeacherSubject(newTs, delIds, uid);
            TeacherInfoLogic teacherInfoLogic = new TeacherInfoLogic();
            var teacherInfo = teacherInfoLogic.GetModelByGuidId(uid);
            teacherInfo.Teacher_LastSubjectID = null;
            int updateCount= teacherInfoLogic.Update(teacherInfo);
            if (updateResult)
            {
                rsp.Success = true;
                rsp.Message = "修改成功";

                // 20170306
                // 删除课程的同时删除对应的操作和共享数据
                foreach (var subjectId in delIds)
                {
                    TeacherExamClassLogic teacherExamClassLogic = new TeacherExamClassLogic(subjectId);
                    updateResult = teacherExamClassLogic.ClearTeacherData(uid);
                }
            }
            else
            {
                rsp.Success = false;
                rsp.Message = "修改失败";
            }
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
    }
}
