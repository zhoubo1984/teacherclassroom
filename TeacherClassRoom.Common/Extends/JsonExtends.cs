﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Extends
{
    public static class JsonExtends
    {
        public static string ToJson<T>(this T obj)
            where T : class,new()
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T JsonToModel<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}
