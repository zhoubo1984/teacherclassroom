﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TeacherClassRoom.Web.Helpers;
using TeacherClassRoom.Web.Logic;
using TeacherClassRoom.Web.Models;
using TeacherClassRoomModel.RequestModel;
using TeacherClassRoomModel.ViewModel;

namespace TeacherClassRoom.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(AuthUserInfo userinfo)
        {
            if (userinfo.UserId == null)
            {
                return HttpNotFound("缺少初始信息");
            }
            SubjectInfoHelper.SetInfo(userinfo);
            ViewBag.UserId = userinfo.UserId.ToString();
            return View();
        }

        public ActionResult Play([FromUri]VideoInfoRequest video)
        {
            video.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
            VideoInfoLogic logic = new VideoInfoLogic();
            var rsp = logic.GetVideo(video);
            var videoinfoviewmodel = new VideoInfoViewModel();
            if(rsp.Video!=null)
            {
                videoinfoviewmodel = rsp.Video;
                ViewBag.VideoMPath = string.Format("/Videos/{0}/playlist.m3u8", rsp.Video.VideoPath).Replace(@"\", "/");// videopath;
                ViewBag.VideoPPath = string.Format("/Videos/{0}/video.mp4", rsp.Video.VideoPath).Replace(@"\", "/");// videopath;
                ViewBag.PicPath = string.Format("/Videos/{0}/video.jpg", rsp.Video.VideoPath).Replace(@"\", "/");// videopath;
                ViewBag.VideoName = rsp.Video.VideoName;
                ViewBag.VideoAuthor = rsp.Video.VideoAuthor;
                ViewBag.VideoAddDate = rsp.Video.VideoAddDate.Value.ToString("yyyy年MM月dd日");
                ViewBag.VideoID = rsp.Video.VideoID;
                ViewBag.TeacherIntroduce = rsp.Video.TeacherDescription;

                //更新视频播放次数
                var updatersp = logic.UpdatePlayCountVideo(new VideoUpdatePlayCountRequest() { VideoID = video.VideoID });
                var txt = updatersp.ResponseText;
            }
            var userinfo = SubjectInfoHelper.GetInfo();
            ViewBag.UserId = userinfo.UserId.ToString();
            return View(videoinfoviewmodel);
        }

        public ActionResult MyCollect()
        {
            var userinfo = SubjectInfoHelper.GetInfo();
            ViewBag.UserId = userinfo.UserId.ToString();
            return View();
        }
    }
}
