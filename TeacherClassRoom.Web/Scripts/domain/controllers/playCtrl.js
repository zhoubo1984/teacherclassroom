﻿angular.module("playApp").controller("playCtrl", function ($scope, basalService) {

    $scope.playedlist = [];
    $scope.conditionSign = 0;
    var userid = $("#hiduserid").val();
    basalService.userid = userid;
    $scope.playInit = function () {
        $scope.videoid = $("#hidvideoid").val();
    }

    var list = basalService.loadPlayedList();
    for (var i = 0; i < list.length; i++) {
        if (((new Date()) - (new Date(list[i].playdate))) / (24 * 60 * 60 * 1000) <= 90) {
            $scope.playedlist.push(list[i]);
        }
    }

    $scope.clearPlaylist = function () {
        var savelist = basalService.clearPlayedListWithout($scope.videoid);
        $scope.playedlist = savelist;
        if (parent.refreshPlayedList) {
            parent.refreshPlayedList();
        }
        layer.msg("清空完成", { icon: 1 });
    };

    $scope.removePlayedItem = function (video) {

        if (video.videoid == $scope.videoid) {
            layer.alert("正在播放，无法删除", { icon: 2 });
            return;
        }

        basalService.removePlayedItem(video);
        var index = 0;
        for (var i = 0; i < $scope.playedlist.length; i++) {
            if ($scope.playedlist[i].videoid == video.videoid) {
                index = i;
                break;
            }
        }
        $scope.playedlist.splice(index, 1);
    }

    $scope.collectVideo = function () {

        var collecttype = $("#imgCollect").attr("collect");
        if (collecttype == "0") {
            basalService.collectVideo($scope.videoid).success(function (data) {
                if (data.ResponseCode == 1000) {
                    layer.msg("收藏成功！", { icon: 1 });
                    $("#imgCollect").attr("src", "/Images/36.png").attr("collect", "1");
                    parent.refresh();
                    parent.parent.refresh();
                }
                else {
                    layer.msg(data.ResponseText, { icon: 5 });
                }
            }).error(function (a, b, c) {
                alert("收藏服务异常，请重试。");
            });
        }
        else {
            basalService.deleteCollect([$scope.videoid]).success(function (data) {
                if (data.ResponseCode == 1000) {
                    layer.msg("取消收藏成功！", { icon: 1 });
                    $("#imgCollect").attr("src", "/Images/a39.png").attr("collect", "0");
                    parent.refresh();
                    parent.parent.refresh();
                }
                else {
                    layer.msg(data.ResponseText, { icon: 5 });
                }
            }).error(function (a, b, c) {
                alert("收藏服务异常，请重试。");
            });
        }
    }

    $scope.recommendVideo = function () {
        basalService.recommendVideo($scope.videoid).success(function (data) {

            if (data.ResponseCode == 1000) {
                layer.msg("推荐成功！", { icon: 1 });
                parent.refresh();
                parent.parent.refresh();
                $("#imgRecommend").hide();
            }
            else {
                layer.msg(data.ResponseText, { icon: 5 });
            }

        }).error(function (a, b, c) {
            alert("收藏服务异常，请重试。");
        });
    };

    $scope.showIntroduce = function () {

        var content = $("#teacherContainer").html();
        if (content.length == 0) {
            layer.msg('暂无简介', { icon: 5 });
            return;
        }

        layer.open({
            type: 4,
            tips: 1,
            shadeClose:true,
            content: [$("#teacherContainer").html(), '#imgIntroduce']
        });
    }

    $scope.playVideo = function (video) {
        
        //layer.load();
        //window.location.href = "/Home/Play?videoid=" + video.videoid;

        parent.replay(video.videoid);
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭

        //var videoMPath = '/Videos/' + video.videopath + '/playlist.m3u8';
        //var videoPPath = '/Videos/' + video.videopath + '/video.mp4';
        //var videoPicPath = '/Videos/' + video.videopath + '/video.jpg';
        //videoMPath = videoMPath.replace(/\\/g, "\/");
        //videoPPath = videoPPath.replace(/\\/g, "\/");
        //videoPicPath = videoPicPath.replace(/\\/g, "\/");
        //$scope.conditionSign = 0;
        //BeginPlay(videoMPath, videoPPath);

        //$('video').attr("poster", videoPicPath);
        //layer.msg("切换成功，点击视频播放");
    }
});