﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 更新返回结果
    /// </summary>
    public class UpdateResponse
    {
        /// <summary>
        /// 操作结果
        /// </summary>
        public bool Success { set; get; }

        /// <summary>
        /// 描述信息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public string Data { set; get; }
    }
}