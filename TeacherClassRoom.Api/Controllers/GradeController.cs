﻿using EduPlatform.Logic.System;
using EduPlatform.Framework;
using EduPlatform.ServiceModel;
using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace TeacherClassRoom.Api.Controllers
{
    public class GradeController : ApiController
    {
        /// <summary>
        /// 返回学科
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>

        [HttpPost]
        public GradeInfoResponse Get(GradeInfoRequest requst)
        {
            var response = new GradeInfoResponse();
            try
            {
                GradeInfoLogic gradelogic = new GradeInfoLogic();
                var grades = gradelogic.GetGradeInfoViewModels();
                if(grades.IsHasData())
                {
                    response.Grades = grades.OrderBy(item=>item.Index).ToList();
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = grades.Count;
                }
                else
                {
                    response.Grades = new List<GradeInfoViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch (Exception ex)
            {
                response.Grades = new List<GradeInfoViewModel>();
                response.ResponseText = "获取失败！";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }

            return response;
        }

        [HttpPost]
        /// <summary>
        /// 返回年级
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        
        public SubGradeInfoResponse GetSubjectGradeInfo(SubGradeInfoRequest request)
        {
            var response = new SubGradeInfoResponse();
            try
            {
                EduPlatform.Logic.System.SubGradeInfoLogic subgradeinfologic = new EduPlatform.Logic.System.SubGradeInfoLogic();
                var subgradeinfos = subgradeinfologic.GetGradeSubGradeInfoViewModels().Where(item => item.GradeId == request.GradeId).ToList();
                if (subgradeinfos.IsHasData())
                {
                    response.SubGradeInfos = subgradeinfos;
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = subgradeinfos.Count;
                }
                else
                {
                    response.SubGradeInfos = new List<GradeSubGradeInfoViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch(Exception ex)
            {
                response.SubGradeInfos = new List<GradeSubGradeInfoViewModel>();
                response.ResponseText = "获取失败";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }
            

            return response;
        }

        [HttpPost]
        public SubGradeInfoResponse GetAllSubjectGradeInfo()
        {
            var response = new SubGradeInfoResponse();
            try
            {
                EduPlatform.Logic.System.SubGradeInfoLogic subgradeinfologic = new EduPlatform.Logic.System.SubGradeInfoLogic();
                var subgradeinfos = subgradeinfologic.GetGradeSubGradeInfoViewModels();
                if (subgradeinfos.IsHasData())
                {
                    response.SubGradeInfos = subgradeinfos;
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = subgradeinfos.Count;
                }
                else
                {
                    response.SubGradeInfos = new List<GradeSubGradeInfoViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch (Exception ex)
            {
                response.SubGradeInfos = new List<GradeSubGradeInfoViewModel>();
                response.ResponseText = "获取失败";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }


            return response;
        }
    }
}
