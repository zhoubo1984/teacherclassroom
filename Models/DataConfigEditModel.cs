﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 系统配置信息编辑实体
    /// </summary>
    public class DataConfigEditModel:DataConfigAddModel
    {
        /// <summary>
        /// 配置id
        /// </summary>
        public string ConfigId { set; get; }
    }
}