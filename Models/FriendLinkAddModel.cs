﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class FriendLinkAddModel
    {
        [Required(ErrorMessage = "必填")]
        public string LinkName { set; get; }

        [Required(ErrorMessage = "必填")]
        public string LinkHref { set; get; }

        
        public int LinkIndex { set; get; }
    }
}