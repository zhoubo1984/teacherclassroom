﻿/// <reference path="E:\workdocs\eduplatform\eduplatform.serviceapi\source\EduPlatform.Manage\assets/js/jquery-1.10.2.min.js" />
/// <reference path="E:\workdocs\eduplatform\eduplatform.serviceapi\source\EduPlatform.Manage\assets/js/bootbox.min.js" />

function ajaxCall(obj) {
    $.ajax({
        url: obj.url,
        type: 'POST',
        dateType: 'json',
        data: obj.data,
        success: obj.success,
        error:obj.error
    });
}