﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using TeacherClassRoom.Web.Models;
//using TeacherClassRoom.Common.Extends;

namespace TeacherClassRoom.Web.Helpers
{
    public class SubjectInfoHelper
    {
        public static void SetInfo(AuthUserInfo sginfo)
        {
            var cookievalue = HttpUtility.UrlEncode(sginfo.ToJsonStr(), Encoding.UTF8);
            HttpCookie authCookie = new HttpCookie("sginfo", cookievalue)
            {
                HttpOnly = true,
                Expires = DateTime.MaxValue
            };
            HttpContext.Current.Response.Cookies.Remove(authCookie.Name);
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        public static AuthUserInfo GetInfo()
        {
            var context = HttpContext.Current;
            var cookie = context.Request.Cookies["sginfo"];
            var cookievalue = HttpUtility.UrlDecode(cookie.Value);
            var subjectgradeinfo = cookievalue.JsonToModel<AuthUserInfo>();
            return subjectgradeinfo;
        }
    }
}