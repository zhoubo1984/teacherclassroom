﻿/// <reference path="E:\workdocs\eduplatform\eduplatform.serviceapi\source\EduPlatform.Manage\assets/js/jquery-1.10.2.min.js" />
/// <reference path="common.js" />

// 用户审核
function verifyUser(uid) {
    layer.confirm("确定审核通过吗?", {
        btn: ["确定","取消"]
    }, function () {
        var senddata = {
            url: '/user/verifyuser',
            data: { uid: uid },
            success: function (d) {
                if (d.Success) {
                    layer.msg('审核成功', { icon: 1 });
                    search(1);
                }
                else {
                    layer.msg('审核失败', { icon: 2 });
                }
            }
        };

        ajaxCall(senddata);
        
    },function() {
        
    });
}