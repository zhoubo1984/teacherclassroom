﻿angular.module("collectApp").controller("collectCtrl", function ($scope, basalService) {

    $scope.pageindex = 1;
    var pagesize = 20;
    $scope.ischoose = 0
    $scope.searchedlist = [];
    $scope.conditionSign = 0;
    $scope.videolist = [];
    $scope.choosesubjectcode = '';
    $scope.videoquery = {};
    $scope.videoquery.videoname = ''
    $scope.select_all = false;
    $scope.subjectvideocountinfo = {};

    var userid = $("#hiduserid").val();
    basalService.userid = userid;

    $scope.setChooseMode = function (mode) {
        $scope.ischoose = mode;
    }

    $scope.removeSearchedItem = function (search) {
        basalService.removeSearchedItem(search);
        var index = 0;
        for (var i = 0; i < $scope.searchedlist.length; i++) {
            if ($scope.searchedlist[i].id == search.id) {
                index = 0;
                break;
            }
        }
        $scope.searchedlist.splice(index, 1);
    };

    $scope.initSearchedList = function () {
        $scope.videoquery.videoname = "";
        $scope.searchedlist.length = 0;
        var list = basalService.loadSearchedList();
        //只保留10条记录
        for (var i = 0; i < list.length; i++) {
            $scope.searchedlist.push(list[i]);
        }
    };
    $scope.initSearchedList();

    $scope.manueSearch = function () {
        var videoname = $.trim($scope.videoquery.videoname);
        if (videoname.length > 0) {
            basalService.storeSearched(videoname);
        }
        
        $scope.searchCollectVideo();

        $scope.conditionSign = 0;
    }

    $scope.closeSearch = function () {
        $scope.conditionSign = 0;
    }

    $scope.selectSearchItem = function (search) {
        $scope.videoquery.videoname = search;
    }
    
    $scope.changeSubject = function (subjectcode) {
        $scope.choosesubjectcode = subjectcode;

        $scope.pageindex = 1;
        $scope.searchCollectVideo();
    }

    $scope.setSelectAll = function () {
        $scope.select_all = !$scope.select_all;
    }

    $scope.removeCollect = function () {
        var collectids = [];
        //获取所有选中的值
        $("input[name='checkbox']:checkbox:checked").each(function () {
            collectids.push($(this).val());
        })
        if (collectids.length == 0) {
            layer.alert('请先选择收藏视频');
            return;
        }

        basalService.deleteCollect(collectids).success(function (data) {
            if (data.ResponseCode == 1000) {
                //删除集合列表
                $scope.videolist = $scope.videolist.filter(function (video) {
                    return $.inArray(video.VideoID, collectids) == -1;
                })

                layer.msg("删除成功", { icon: 1 });
                setTimeout(function () {
                    var currpage = $scope.pageindex;
                    for (var i = 1; i <= currpage; i++) {
                        $scope.pageindex = i;
                        $scope.searchCollectVideo();
                    }
                }, 1000);
                //更新服容器中的首页的收藏信息
                parent.refresh();
            }
            else {
                layer.msg("删除失败请重试", { icon: 2 });
            }
        }).error(function (a, b, c) {
            layer.msg("请求失败请重试", { icon: 2 });
        })
    };

    $scope.playVideo = function (video) {

        basalService.storePlayed(video);//先存储该视频到播放列表
        $scope.goplay(video.VideoID);
    };

    $scope.recommendVideo = function (video) {
        if (video.UserRecommend != 0) {
            return;
        }

        basalService.recommendVideo(video.VideoID).success(function (data) {

            if (data.ResponseCode == 1000) {
                video.UserRecommend++;
                video.RecommendCount++;
                layer.msg("推荐成功！", { icon: 1 });
                parent.refresh();
            }
            else {
                layer.msg(data.ResponseText, { icon: 5 });
            }

        }).error(function (a, b, c) {
            alert("推荐服务异常，请重试。");
        });
    };

    $scope.goplay = function (videoid) {
        layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ['100%', '100%'],
            content: "/Home/Play?videoid=" + videoid //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    }

    $scope.searchCollectVideo = function () {
        var loadindex = layer.load();
        //加载视频列表

        basalService.getCollectList($scope.choosesubjectcode, $scope.videoquery.videoname, $scope.pageindex, pagesize).success(function (data) {
            if (data.ResponseCode == 1000) {
                if ($scope.pageindex == 1)
                    $scope.subjectvideocountinfo.videolist = data.Videos;
                else
                    $scope.subjectvideocountinfo.videolist = $scope.subjectvideocountinfo.videolist.concat(data.Videos);
                $scope.subjectvideocountinfo.totalcount = data.TotalSubjectVideoCount;
                $scope.subjectvideocountinfo.subjectvideocount = data.SubjectVideoCount;
            }
            else {
                layer.msg("视频信息服务异常，请重试。", { icon: 5 });
            }
        }).error(function (a, b, c) {
            layer.msg("收藏信息服务异常，请重试。", { icon: 5 });
        }).then(function () {
            layer.close(loadindex);
        });
    };

    $scope.clearSearched = function () {
        basalService.clearSearchedList();
        $scope.searchedlist.length = 0;
    }

    $scope.searchPress = function (event) {
        var key_code = event.keyCode;
        if (key_code == 13) {
            $scope.manueSearch();
        }
    }

    $scope.searchCollectVideo();
});