﻿using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeacherClassRoom.Web.Models;

namespace TeacherClassRoom.Web.Helpers
{
    public class TreeJsonConverter
    {
        public static List<TreeJsonModel> Convert(List<KnowledgeInfoViewModel> knowledges)
        {
            List<TreeJsonModel> list = new List<TreeJsonModel>();
            var parentlist = knowledges.Where(k => string.IsNullOrEmpty(k.PID)).ToList();
            foreach(var k in parentlist)
            {
                TreeJsonModel tm = new TreeJsonModel();
                tm.id = k.KnowledgeID.ToString();
                tm.text = k.KnowledgeValue;
                tm.tags.Add(k.KnowledgeID.ToString());
                tm.state = new TreeJsonState() { @checked = false, expanded = true };
                Recus(tm, knowledges);

                list.Add(tm);
            }

            return list;
        }

        private static void Recus(TreeJsonModel parentnode,List<KnowledgeInfoViewModel> knowledges)
        {
            var childlist = knowledges.Where(k => k.PID==parentnode.tags[0]).ToList();
            foreach(var k in childlist)
            {
                TreeJsonModel tm = new TreeJsonModel();
                tm.id = k.KnowledgeID.ToString();
                tm.text = k.KnowledgeValue;
                tm.tags = new List<string>();
                tm.tags.Add(k.KnowledgeID.ToString());
                tm.state = new TreeJsonState() { @checked = false, expanded = true };
                parentnode.nodes.Add(tm);
                Recus(tm, knowledges);
            }
        }
    }
}