﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 数据库路由编辑实体
    /// </summary>
    public class DataRouteEditModel : DataRouteAddModel
    {
        /// <summary>
        /// routeid
        /// </summary>
        public string RouteID { set; get; }
    }
}