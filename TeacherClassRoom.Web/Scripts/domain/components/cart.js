﻿//试题购物车模块
angular.module("cart", ['dataServices'])
    .factory("cart", function (examDataService) {

        //保存购物车试题数据
        var cartData = [];
        var parnt = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        return {
            //临时存储在data中
            save: function (cartkey) {
                $('body').data(cartkey, cartData);
            },
            //获取当前添加的试题集合
            getExams:function(){
                return cartData;
            },
            //重新加载存储的试题集合
            reload:function(cartkey){
                cartData = angular.isDefined($('body').data(cartkey)) ? $('body').data(cartkey) : [];
            },
            //删除购物车中所有的试题
            removeAll: function () {
                cartData.length = 0;
            },
            //删除购物车中某题型
            removeQuestionType: function (questiontypeid) {
                var j = 0;
                var isexists = false;
                for (j; j < cartData.length; j++) {
                    if (cartData[j].Key == questiontypeid) {
                        isexists = true;
                        break;
                    }  
                }
                if (isexists) {
                    cartData.splice(j, 1);
                    //调整ordeindex
                    for (var i = j; i < cartData.length; i++) {
                        cartData[i].OrderIndex = cartData[i].OrderIndex - 1;
                    }
                }
            },
            
            //获取购物车中的试题总量
            totalExamCount: function () {
                var totalcount = 0;
                $.each(cartData, function (index, val) {
                    totalcount += val.Items.length;
                });

                return totalcount;
            },
            //获取购物车中总的试题分数
            totalExamScore: function () {
                var totalscore = 0;
                $.each(cartData, function (index, val) {
                    $.each(val.Items, function (ind, va) {
                        if (va.Score != "" && parnt.test(va.Score)) {
                            totalscore += parseFloat(va.Score);
                        }
                    });
                });

                return totalscore;
            },
            
            //计算某个题型下的试题总分
            totalQuestionTypeExamScore : function (questionkey) {
                var totalscore = 0;
                for (var i = 0; i < cartData.length; i++) {
                    if (cartData[i].Key == questionkey) {
                        for (var j = 0; j < cartData[i].Items.length; j++) {
                            if (cartData[i].Items[j].Score != "" && parnt.test(cartData[i].Items[j].Score)) {
                                totalscore += parseFloat(cartData[i].Items[j].Score);
                            }
                        }
                        break;
                    }
                }
                return totalscore;
            },

            //向上移动大题
            moveItemGroupDown: function (orderindex) {

                var orderitem = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex }).FirstOrDefault();
                var ordernextitem = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex+1 }).FirstOrDefault();

                orderitem.OrderIndex = ordernextitem.OrderIndex;
                ordernextitem.OrderIndex = orderindex;
            },

            //向下移动大题
            moveItemGroupUp: function (orderindex) {

                var orderitem = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex }).FirstOrDefault();
                var ordernextitem = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex -1 }).FirstOrDefault();

                orderitem.OrderIndex = ordernextitem.OrderIndex;
                ordernextitem.OrderIndex = orderindex;
            },

            //向下移动小题 orderindex：所属大题的序号 number：所属小题的序号
            moveItemDown: function (orderindex,number) {

                var items = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex }).FirstOrDefault().Items;
                var orderitem = Enumerable.From(items).Where(function (x) { return x.Number == number }).FirstOrDefault();
                var ordernextitem = Enumerable.From(items).Where(function (x) { return x.Number == number + 1 }).FirstOrDefault();

                orderitem.Number = ordernextitem.Number;
                ordernextitem.Number = number;
            },

            //向上移动小题
            moveItemUp: function (orderindex,number) {

                var items = Enumerable.From(cartData).Where(function (x) { return x.OrderIndex == orderindex }).FirstOrDefault().Items;
                var orderitem = Enumerable.From(items).Where(function (x) { return x.Number == number }).FirstOrDefault();
                var ordernextitem = Enumerable.From(items).Where(function (x) { return x.Number == number - 1 }).FirstOrDefault();

                orderitem.Number = ordernextitem.Number;
                ordernextitem.Number = number;
            },

            //检查试题分数合法性
            checkCartexam: function () {
                var isvalid = true;
                $.each(cartData, function (index, val) {
                    $.each(val.Items, function (ind, va) {
                        if (!parnt.test(va.Score)) {
                            isvalid = false;
                        }
                    })
                });

                return isvalid;
            },

            //删除购物车中的某个试题
            removeExam: function (examid) {
                var groupindex = -1;
                var itemindex = -1;
                $.each(cartData, function (index, val) {
                    groupindex = index;
                    $.each(val.Items, function (i, v) {
                        if (v.ExamId == examid) {
                            itemindex = i;
                            return false;
                        }
                    });
                    if (itemindex != -1)
                        return false;
                });

                if (groupindex != -1 && itemindex != -1) {
                    cartData[groupindex].Items.splice(itemindex, 1);

                    //重新调整小题编号
                    for (var i = 0; i < cartData[groupindex].Items.length; i++) {
                        cartData[groupindex].Items[i].Number = i + 1;
                    }

                    //如果删除后所在大题内无题，则删除大题
                    if (cartData[groupindex].Items.length == 0) {
                        this.removeQuestionType(cartData[groupindex].Key);
                    }
                }
            },


            //判断试题是否存在于购物车
            isInCart: function (examid) {
                var isexit = false;
                $.each(cartData, function (index, val) {
                    $.each(val.Items, function (i, v) {
                        if (v.ExamId == examid) {
                            isexit = true;
                            return false;
                        }
                    });
                });

                return isexit;
            },

            //添加试题到购物车
            addExam: function (exam) {
                
                var isnewtype = true;       //是否是新的题型
                var isexit = this.isInCart(exam.ExamId);         //试题是否已经存在
                var totalitemcount = this.totalExamCount();     //当前已选的试题数量

                //如果存在则删除并返回
                if (isexit) {
                    this.removeExam(exam.ExamId);
                    return -1;
                }
                if (totalitemcount >= 50) {
                    layer.alert('添加失败，所有题型试题总共不得超过50道。', { icon: 2 });
                    return -1;
                }

                var convertexam = examDataService.createTestExamViewModel(exam);
                convertexam.ExamId = exam.ExamId;
                convertexam.Number = 1;                                          //相关小题题号
                convertexam.Score = 1;

                //加入试题集合
                $.each(cartData, function (index, val) {
                    if (val.Key == exam.QuestionTypeId) {
                        isnewtype = false;
                        val.Items.push(convertexam);
                        convertexam.Number = val.Items.length;
                        convertexam.Score = val.PerScore;
                        return false;
                    }
                });
                //如果是一个新题型的则增加一个新题型对象
                if (isnewtype) {
                    //Key :题型编号 PerScore：单题分数 QuestionTypeName：题型名称 OrderIndex ：大题序号Items 题型下试题
                    cartData.push({ Key: convertexam.QuestionTypeId, PerScore: convertexam.Score, QuestionTypeName: convertexam.QuestionTypeName, OrderIndex: cartData.length + 1, Items: [convertexam] });
                }
                
                return 1;
            },

            //更换试题
            changeExam: function (newexam, oldexam) {

                var isnewtype = true;       //是否是新的题型
                var isexit = this.isInCart(newexam.ExamId);         //试题是否已经存在

                //如果存在则提示错误并返回
                if (isexit) {
                    layer.alert('该试题已经存在。', { icon: 2, zIndex: 29991015 });
                    return -1;
                }

                //初始化新试题格式
                var convertexam = examDataService.createTestExamViewModel(newexam);
                convertexam.condition = oldexam.condition;
                convertexam.ExamId = newexam.ExamId;
                convertexam.Number = oldexam.Number;                                          //相关小题题号
                convertexam.Score = 1;

                //加入试题集合 获取老试题在购物车中的位置 ，然后添加到新试题对应的题型下，如果是新题型则创建新题型并将试题放入，最后删除被替换的试题
                $.each(cartData, function (index, val) {

                    if (val.Key == newexam.QuestionTypeId) {
                        isnewtype = false;
                        val.Items.push(convertexam);
                        convertexam.Number = oldexam.QuestionTypeId == val.Key ? oldexam.Number : val.Items.length;
                        convertexam.Score = val.PerScore;

                        return false;
                    }
                });
                //如果是一个新题型的则增加一个新题型对象
                if (isnewtype) {
                    //Key :题型编号 PerScore：单题分数 QuestionTypeName：题型名称 OrderIndex ：大题序号Items 题型下试题
                    cartData.push({ Key: convertexam.QuestionTypeId, PerScore: convertexam.Score, QuestionTypeName: convertexam.QuestionTypeName, OrderIndex: cartData.length + 1, Items: [convertexam] });
                }

                this.removeExam(oldexam.ExamId);
                layer.msg('替换成功', { icon: 1, zIndex: 29991015 });
            }
        }
    });