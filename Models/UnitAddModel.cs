﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 单位信息添加实体
    /// </summary>
    public class UnitAddModel
    {
        /// <summary>
        /// 单位名称
        /// </summary>
        [Required(ErrorMessage="必填")]
        public string UnitName { set; get; }

        /// <summary>
        /// 省份
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public int Province { set; get; }

        /// <summary>
        /// 城市编号
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public int City { set; get; }

        /// <summary>
        /// 上级单位编号
        /// </summary>
        public string ParentUnit { set; get; }

        /// <summary>
        /// 学段
        /// </summary>
        [Required(ErrorMessage="必填")]
        public List<Guid> Grade { set; get; }

        /// <summary>
        /// 单位编号
        /// </summary>
        public Guid UnitId { set; get; }

        /// <summary>
        /// 修改的单位管理员登录名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 修改的管理员的登录名称
        /// </summary>
        public string AccountLoginName { get; set; }
    }
}