﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Sdk
{
    public class TCRSckFactory
    {
        static string _baseurl;

        public static void Init(string baseurl)
        {
            _baseurl = baseurl;
        }

        public static string BaseUrl
        {
            get { return _baseurl; }
        }

        public static readonly GradeInfoSdk GradeInfoSdk = new GradeInfoSdk();
    }
}
