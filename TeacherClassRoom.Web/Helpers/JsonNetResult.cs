﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeacherClassRoom.Web.Helpers
{
    public class JsonNetResult : JsonResult  
    {

        private object _data;
        public JsonNetResult(object data)
        {
            this.Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;
            if (this.Data != null)
            {
                Newtonsoft.Json.JsonSerializerSettings setting = new JsonSerializerSettings();
                setting.DateFormatString = "yyyy-MM-dd";
                response.Write(JsonConvert.SerializeObject(Data, setting));
            }
        }  
    }
}