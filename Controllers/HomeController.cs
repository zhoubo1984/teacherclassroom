﻿using EduPlatform.Logic.System;
using EduPlatform.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Security;
using EduPlatform.Manage.Infrastructure;
using EduPlatform.Framework;

namespace EduPlatform.Manage.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult GetVerifyCode()
        {
            ValidateCode vCode = new ValidateCode();
            string code = vCode.CreateValidateCode(5);
            Session["ValidateCode"] = code;
            byte[] bytes = vCode.CreateValidateGraphic(code);
            return File(bytes, @"image/jpeg");
        }

        [HttpPost]
        public ActionResult Login(UserLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            //AccountInfoLogic accountLogic = new AccountInfoLogic();
            TeacherClassRoomLogic.Imp.AccountInfoLogic accountLogic = new TeacherClassRoomLogic.Imp.AccountInfoLogic();

            var accountDomain = accountLogic.GetAccount(model.UserName, Md5Helper.GetMd5Hash(model.Password));

            if (accountDomain != null)
            {
                AccountInfoModel accountInfo = new AccountInfoModel()
                {
                    AccountID = accountDomain.Account_ID.ToString(),
                    LoginName = accountDomain.Account_LoginName,
                    IsAdmin=accountDomain.Account_IsAdmin 
                };

                string accountJson = JsonConvert.SerializeObject(accountInfo);
                var ticket = new FormsAuthenticationTicket(1, accountInfo.LoginName, DateTime.Now, DateTime.Now.AddDays(1), true, accountJson);
                string cookieString = FormsAuthentication.Encrypt(ticket);
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieString);
                authCookie.Expires = ticket.Expiration;
                authCookie.Path = FormsAuthentication.FormsCookiePath;
                Response.Cookies.Add(authCookie);

                return RedirectToAction("Index", "Manage", null);
            }
            return View();
        }

        public ActionResult Page404()
        {
            return View();
        }

        public ActionResult Page500()
        {
            return View();
        }


    }
}
