﻿using EduPlatform.ServiceModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TeacherClassRoom.Common.Log;

namespace TeacherClassRoom.Common.ApiClient
{
    public class ApiHelper
    {
        //static HttpClient httpClient;放弃HttpClient1
        static RestClient httpClient;

        static ApiHelper()
        {
            if (httpClient == null)
            {
                httpClient = new RestClient();
            }
        }

        public static TResponse PostDataToServer<TRequest,TResponse>(string baseurl, string apiurl, TRequest model)
            where TResponse:new()
        {
            httpClient.BaseUrl = new Uri(baseurl);
            RestRequest request = new RestRequest(apiurl, Method.POST);
            if (model != null)
                request.AddJsonBody(model);
            var result = httpClient.Post<TResponse>(request);
            if (result.ErrorException != null)
            {
                throw result.ErrorException;
            }
            return result.Data;
        }

        public static Stream HttpDownloadFile(string url)
        {
            try
            {
                // 设置参数
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //发送请求并获取相应回应数据
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                Stream responseStream = response.GetResponseStream();

                MemoryStream ms = new MemoryStream();
                //创建本地文件写入流

                byte[] bArr = new byte[1024];
                int size = responseStream.Read(bArr, 0, (int)bArr.Length);
                while (size > 0)
                {
                    ms.Write(bArr, 0, size);
                    size = responseStream.Read(bArr, 0, (int)bArr.Length);
                }
                responseStream.Close();
                ms.Position = 0;
                return ms;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string PostFileToServer(string baseurl,string apiurl,List<string> paths,NameValueCollection querystring)
        {
            httpClient.BaseUrl = new Uri(baseurl);
            RestRequest request = new RestRequest(apiurl, Method.POST);
            foreach(string key in querystring.Keys)
            {
                request.AddParameter(key, querystring.Get(key));
            }

            foreach(string path in paths)
            {
                var ext = Path.GetExtension(path);
                Stream stream = File.OpenRead(path);
                byte[] bytecontent = new byte[stream.Length];
                stream.Read(bytecontent,0,bytecontent.Length);
                request.AddFileBytes("files", bytecontent,Path.GetFileName(path).Replace(".docx",ext));
                stream.Close();
            }

            var result = httpClient.Execute(request);
            return result.Content;
        }
    }
}
