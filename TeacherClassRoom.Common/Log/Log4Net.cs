﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Log
{
    public class Log4Net:ILog
    {
        private static readonly log4net.ILog Logerror = LogManager.GetLogger("logerror");
        private static readonly log4net.ILog Logdebug = LogManager.GetLogger("logdebug");
        private static readonly log4net.ILog Loginfo = LogManager.GetLogger("loginfo");

        public Log4Net(string configFileName)
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(configFileName));
        }

        public void LogConfig(string fileName)
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(fileName));
        }

        public void LogInfo(string message)
        {
            Loginfo.Info(message);
        }

        public void LogInfo(string message, Exception ex)
        {
            Loginfo.Info(message, ex);
        }

        public void LogInfo(Exception ex)
        {
            Loginfo.Info(ex);
        }

        public void LogBug(string message)
        {
            Logdebug.Debug(message);
            //throw new Exception("方法尚未实现");
        }

        public void LogBug(string message, Exception ex)
        {
            Logdebug.Debug(message, ex);
        }

        public void LogBug(Exception ex)
        {
            Logdebug.Debug(ex);
        }

        public void LogError(string message, Exception ex)
        {
            Logerror.Error(message, ex);
        }

        public void LogError(string message)
        {
            Logerror.Error(message);
        }

        public void LogError(Exception ex)
        {
            Logerror.Error(ex);
        }
    }
}
