﻿using EduPlatform.Domain.System;
using EduPlatform.Framework;
using EduPlatform.Logic.System;
using EduPlatform.Manage.Infrastructure;
using EduPlatform.Manage.Models;
using EduPlatform.SearchModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Web;
using System.Web.Mvc;

namespace EduPlatform.Manage.Controllers
{
    public class AuthManageController : BaseController
    {
        AuthGroupInfoLogic authgrouplogic = new AuthGroupInfoLogic();
        SystemActionMenuLogic menuLogic = new SystemActionMenuLogic();
        AuthGroupMenuLogic authGroupMenuLogic = new AuthGroupMenuLogic();
        AccountInfoLogic accountLogic = new AccountInfoLogic();
        AccountAuthLogic accountauthLogic = new AccountAuthLogic();
        //
        // GET: /AuthManage/

        public ActionResult Index()
        {
            return View();
        }

        #region 权限组管理
        /// <summary>
        /// 权限组管理显示
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult AuthGroupList()
        {
            return View();
        }

        /// <summary>
        /// 权限组列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AuthGroupResultList(AuthGroupSearchModel search)
        {
            ViewBag.CurrentUrl = "AuthManage/AuthGroupList";
            var authgroup = authgrouplogic.AuthGroupSearch(search);
            return View(authgroup);
        }

        /// <summary>
        /// 权限组添加页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddAuthGroup()
        {
            return View();
        }

        /// <summary>
        /// 添加操作
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddAuthGroup(AuthGroupSearchModel model)
        {
            if (string.IsNullOrEmpty(model.GroupName))
            {
                ViewBag.Msg = "请填写组名称";
                return View();
            }
            T_AuthGroup authgroup = new T_AuthGroup
            {
                AG_ID=Guid.NewGuid(),
                AG_GroupName = model.GroupName,
                AG_AccountID = Guid.Parse(account.AccountId),
                AG_AddDate=DateTime.Now,
                AG_State=1
            };
            bool updateResult = authgrouplogic.Insert(authgroup) > 0;
            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            if (updateResult) { 
              return RedirectToAction("AuthGroupList");
            }
            ViewBag.Msg = "添加失败";
            return View();
        }

        /// <summary>
        /// 删除操作
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AuthGroupDelete(Guid unitId)
        {
            var rsp = new UpdateResponse();
            var unit = authgrouplogic.GetModelByGuidId(unitId);
            unit.AG_State = 0;
            rsp.Success = authgrouplogic.Update(unit) > 0;
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 修改界面
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public ActionResult AuthGroupEdit(Guid ?unitId)
        {
            if (!unitId.HasValue)
            {
                ViewBag.Msg = "请填写组名称";
                return View();
            }
            var unit = authgrouplogic.GetModelByGuidId(unitId.Value);
            if (unit == null)
            {
                ViewBag.Msg = "获取详细信息失败";
                return View();
            }
            AuthGroupInfoModel model = new AuthGroupInfoModel 
            {
                ID=unit.AG_ID,
                GroupName=unit.AG_GroupName,
                UseState=unit.AG_UseState
            };
            return View(model);
        }

        /// <summary>
        /// 修改操作
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AuthGroupEdit(AuthGroupInfoModel model)
        {
            if(string.IsNullOrEmpty(model.GroupName)||model.ID==Guid.Empty)
            {
                ViewBag.Msg = "请填写必要信息";
                return View(model);
            }
            var unit = authgrouplogic.GetModelByGuidId(model.ID);
            unit.AG_UseState=model.UseState;
            unit.AG_GroupName = model.GroupName;
            if (authgrouplogic.Update(unit) > 0)
            {
                return RedirectToAction("AuthGroupList");
            }
            ViewBag.Msg = "修改失败";
            return View(model);
        }

        /// <summary>
        /// 权限组菜单操作
        /// </summary>
        /// <returns></returns>
        public ActionResult AntuGroupMenu(Guid unitId)
        {
            List<T_SystemActionMenu> menusDomain = menuLogic.GetMenus();
            var unit = authGroupMenuLogic.GetModelByGuidId(unitId);
            if (unit != null)
            {
                ViewBag.TreeData = unit.AuthMenu_MenuID;
            }
            return View(menusDomain);
        }

        /// <summary>
        /// 编辑权限菜单
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public ActionResult AddGroupMenu(AuthGroupMenuInfo info)
        {
            var rsp = new UpdateResponse();
            var unit = authGroupMenuLogic.GetModelByGuidId(info.GroupID);
            if (unit == null)
            {
                T_AuthGroupMenu model = new T_AuthGroupMenu 
                {
                    AuthMenu_ID=Guid.NewGuid(),
                    AuthMenu_AddDate=DateTime.Now,
                    AuthMenu_GroupID=info.GroupID,
                    AuthMenu_MenuID=info.MenuID
                };
                bool updateResult = authGroupMenuLogic.Insert(model) > 0;
                if (updateResult)
                {
                    return Json(new { State = "True", Message = "操作成功！" });
                }
               
            }
            else
            {
                unit.AuthMenu_MenuID = info.MenuID;
                if (authGroupMenuLogic.Update(unit) > 0)
                {
                    return Json(new { State = "True", Message = "操作成功！" });
                }
            }
            return Json(new { State = "Fail", Message = "操作失败！" });
        }
        #endregion

        #region 菜单管理

        /// <summary>
        /// 模块管理列表页面
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult ModuleManage()
        {
            IEnumerable<T_SystemActionMenu> menusDomain = menuLogic.GetMenus().Where(t=>t.M_IsParent==1);
            ViewBag.dataList = new SelectList(menusDomain, "M_ID", "M_ActionName");
            return View();
        }

        /// <summary>
        /// 模块查询
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ModuleManageReslutList(SystemActionMenuSearch search)
        {
            ViewBag.CurrentUrl = "AuthManage/ModuleManageReslutList";
            var authgroup = menuLogic.AuthGroupSearch(search);
            return View(authgroup);
        }

        /// <summary>
        /// 添加页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddModule(string type, string unitId)
        {
            if(type=="edit")
            {
                if (string.IsNullOrEmpty(unitId)) return View();
                var unit = menuLogic.GetModelByGuidId(Guid.Parse(unitId));
                IEnumerable<T_SystemActionMenu> menusDomain = menuLogic.GetMenus().Where(t => t.M_IsParent == 1);
                ViewBag.dataList = new SelectList(menusDomain, "M_ID", "M_ActionName", unit.M_PID);
                SystemActionMenuInfo info = new SystemActionMenuInfo
                {
                    ID = unit.M_ID,
                    ActionName=unit.M_ActionName,
                    ActionUrl=unit.M_ActionUrl,
                    ICON=unit.M_ICON,
                    M_Index=unit.M_Index
                };
                return View(info); 
            }
            else
            {
                
                IEnumerable<T_SystemActionMenu> menusDomain = menuLogic.GetMenus().Where(t => t.M_IsParent == 1);
                ViewBag.dataList = new SelectList(menusDomain, "M_ID", "M_ActionName");
                return View(); 
            }
        }

        [HttpPost]
        public ActionResult AddModule(SystemActionMenuInfo model,string Type)
        {
            if (Type.Equals("edit"))
            {
                var unit=menuLogic.GetModelByGuidId(model.ID);
                unit.M_ActionName = model.ActionName;
                unit.M_ActionUrl = model.ActionUrl ?? "#";
                unit.M_PID = model.PID;
                unit.M_ICON = model.ICON;
                unit.M_IsParent = model.IsParent;
                unit.M_Index = model.M_Index;
                if (menuLogic.Update(unit) > 0)
                {
                    return RedirectToAction("ModuleManage");
                }
                ViewBag.Msg = "修改失败";
                return View(model);
            }
            else
            {
                T_SystemActionMenu actionMenu = new T_SystemActionMenu
                {
                    M_ID = Guid.NewGuid(),
                    M_ActionName = model.ActionName,
                    M_ActionUrl = model.ActionUrl ?? "#",
                    M_AddDate = DateTime.Now,
                    M_PID = model.PID,
                    M_ICON = model.ICON,
                    M_IsParent = model.IsParent,
                    M_Index = model.M_Index,
                    M_State = 1
                };
                bool updateResult = menuLogic.Insert(actionMenu) > 0;
                ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
                if (updateResult)
                {
                    return RedirectToAction("ModuleManage");
                }
                ViewBag.Msg = "添加失败";
                return View();
            }
        }

        public JsonResult ModuleDelete(Guid unitId)
        {
            var rsp = new UpdateResponse();
            var unit = menuLogic.GetModelByGuidId(unitId);
            unit.M_State = 0;
            rsp.Success = menuLogic.Update(unit) > 0;
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 用户管理
        [AuthenUrlFilter]
        public ActionResult AccountList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AccountResultList(AccountInfoSearch search)
        {
            ViewBag.CurrentUrl = "AuthManage/AccountResultList";
            var accountList=accountLogic.AccountSearch(search);
            return View(accountList);
        }

        /// <summary>
        /// 用户添加页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AccountAdd()
        {
            return View();
        }

        /// <summary>
        /// 用户添加操作
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AccountAdd(AccountInfo model)
        {
            var existAccount = accountLogic.GetAccount(model.LoginName);
            if (existAccount != null)
            {
                ViewBag.Msg = "用户名已存在,添加失败!";
                return View();
            }

            T_AccountInfo info = new T_AccountInfo 
            { 
                Account_ID=Guid.NewGuid(),
                Account_LoginName=model.LoginName,
                Account_Name=model.Name,
                Account_LoginPwd = Md5Helper.GetMd5Hash(model.LoginPwd),
                Account_State=1,
                Account_AddDate=DateTime.Now,
                Account_IsAdmin=0
            };
            bool updateResult = accountLogic.Insert(info) > 0;
            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            if (updateResult)
            {
                return RedirectToAction("AccountList");
            }
            ViewBag.Msg = "添加失败";
            return View();
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public ActionResult AccountPwdEdit(string unitId)
        {
            if (string.IsNullOrEmpty(unitId)) return View();
            var unit = accountLogic.GetModelByGuidId(Guid.Parse(unitId));
            AccountInfo info = new AccountInfo 
            {
                ID=unit.Account_ID,
                LoginName=unit.Account_LoginName,
                Name=unit.Account_Name
            };
            return View(info);
        }

        /// <summary>
        /// 修改密码操作
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AccountPwdEdit(AccountInfo model)
        {
            var user=accountLogic.GetAccount(model.LoginName,Md5Helper.GetMd5Hash(model.LoginPwd));
            if (user == null)
            {
                ViewBag.Msg = "用户不存在";
                return View(model);
            }
            user.Account_LoginPwd = Md5Helper.GetMd5Hash(model.NewPwd);
            if (accountLogic.Update(user) > 0)
            {
                return RedirectToAction("AccountList");
            }
            ViewBag.Msg = "修改失败";
            return View(model);
        }
        /// <summary>
        /// 修改界面
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public ActionResult AccountEdit(string unitId)
        {
            return AccountPwdEdit(unitId);
        }
        /// <summary>
        /// 修改操作
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AccountEdit(AccountInfo model)
        {
            var user = accountLogic.GetModelByGuidId(model.ID);
            user.Account_Name = model.Name;
            if (accountLogic.Update(user) > 0)
            {
                return RedirectToAction("AccountList");
            }
            ViewBag.Msg = "修改失败";
            return View(model);
        }

        /// <summary>
        /// 用户删除操作
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public JsonResult AccountDelete(Guid accountId)
        {
            var rsp = new UpdateResponse();
            var accountInfo = accountLogic.GetModelByGuidId(accountId);
            if (accountInfo.Account_IsAdmin == 1)
            {
                rsp.Success = false;
                rsp.Message = "超级管理员不能删除";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }
            accountInfo.Account_State = 0;
            rsp.Success = accountLogic.Update(accountInfo) > 0;
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccountAuth(Guid unitId)
        {
            List<AuthGroupInfoModel> authGroup = authgrouplogic.GetAuthGroup()
                .Select(t => new AuthGroupInfoModel { 
                    ID=t.AG_ID,
                    GroupName=t.AG_GroupName  
            }).ToList();
            var unit = accountauthLogic.GetModelByGuidId(unitId);
            if (unit != null)
            {
                ViewBag.TreeData = unit.UserAuth_AuthID;
            }
            return View(authGroup);
        }
        /// <summary>
        /// 编辑权限菜单
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public ActionResult AddUserAuth(AccountAuthInfo info)
        {
            var rsp = new UpdateResponse();
            var unit = accountauthLogic.GetModelByGuidId(info.UserID);
            if (unit == null)
            {
                T_AccountAuth model = new T_AccountAuth
                {
                    UserAuth_ID = Guid.NewGuid(),
                    UserAuth_AddDate = DateTime.Now,
                    UserAuth_UserID = info.UserID,
                    UserAuth_AuthID = info.AuthID
                };
                bool updateResult = accountauthLogic.Insert(model) > 0;
                if (updateResult)
                {
                    return Json(new { State = "True", Message = "操作成功！" });
                }

            }
            else
            {
                unit.UserAuth_AuthID = info.AuthID;
                if (accountauthLogic.Update(unit) > 0)
                {
                    return Json(new { State = "True", Message = "操作成功！" });
                }
            }
            return Json(new { State = "Fail", Message = "操作失败！" });
        }
        #endregion
    }
}
