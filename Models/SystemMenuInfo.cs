﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class SystemMenuInfo
    {
        public Guid MenuID { set; get; }

        public string MenuName { set; get; }

        public Guid? Pid { set; get; }

        public string Url { set; get; }

        public string Icon { set; get; }

        public bool IsOpen { set; get; }

        public bool IsActive { set; get; }

        public bool IsParent { set; get; }

        public List<SystemMenuInfo> SubMenus { set; get; }
    }
}