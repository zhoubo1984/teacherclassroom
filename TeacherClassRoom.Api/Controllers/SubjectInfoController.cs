﻿using EduPlatform.SearchModel;
using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeacherClassRoomLogic.Imp;
using EduPlatform.Framework;
using TeacherClassRoomModel.RequestModel;
using EduPlatform.ServiceModel;

namespace TeacherClassRoom.Api.Controllers
{
    public class SubjectInfoController : ApiController
    {
        /// <summary>
        /// 获取学科信息
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public SubjectInfoResponse Get(SubjectGradeRequest request)
        {
            
            var response = new SubjectInfoResponse();
            try
            {
                SubjectInfoLogic subjectinfologic = new SubjectInfoLogic();
                var subjects = subjectinfologic.SubjectInfoSearch(request.GradeId);
                if (subjects.IsHasData())
                {
                    response.Subjects = subjects;
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = subjects.Count;
                }
                else
                {
                    response.Subjects = new List<GradeSubjectViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch (Exception ex)
            {
                response.Subjects = new List<GradeSubjectViewModel>();
                response.ResponseText = "获取失败！";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }
            return response;
        }

        [HttpPost]
        public SubjectInfoResponse GetAll(SubjectGradeRequest request)
        {
            var response = new SubjectInfoResponse();
            try
            {
                SubjectInfoLogic subjectinfologic = new SubjectInfoLogic();
                var subjects = (from u in subjectinfologic.Fetch()
                                where u.Subject_State==1
                                select new GradeSubjectViewModel
                                {
                                    GradeID=u.Subject_GradeID,
                                    Index = u.Subject_Index,
                                    SubjectCode = u.Subject_Code,
                                    SubjectID = u.Subject_ID,
                                    SubjectName=u.Subject_Name
                                }).ToList();

                if(subjects.IsHasData())
                {
                    response.Subjects = subjects;
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = subjects.Count;
                }
                else
                {
                    response.Subjects = new List<GradeSubjectViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch (Exception ex)
            {
                response.Subjects = new List<GradeSubjectViewModel>();
                response.ResponseText = "获取失败！";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }
            return response;
            
        }
    }
}
