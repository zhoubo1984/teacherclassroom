﻿//字符串过长截断
angular.module("domainfilters", []).filter("longStringFilter", function () {
    return function (value,iscut) {
        if (angular.isString(value)) {
            if (!iscut)//是否需要裁断
                return value.replace(/\|/g, '，');
            else
                return value.replace(/\|/g, '，').length > 8 ? value.replace(/\|/g, '，').substring(0, 7) + '...' : value.replace(/\|/g, '，')
        }
        return value;
    }
});