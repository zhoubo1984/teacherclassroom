﻿using EduPlatform.ServiceModel;
using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeacherClassRoomLogic.Imp;
using EduPlatform.Framework;

namespace TeacherClassRoom.Api.Controllers
{
    public class BookInfoController : ApiController
    {
        [HttpPost]
        public BookSearchResponse Get(BookSearchRequest request)
        {
            var response = new BookSearchResponse();

            if (request == null || request.SubjectID == null||request.PressID==null)
            {
                response.Books = new List<BookDetailViewModel>();
                response.ResponseCode = ResponseCode.ParamError;
                response.ResponseText = "请求参数无效";
                response.DataCount = 0;
                return response;
            }
            BookInfoLogic booklogic = new BookInfoLogic();
            var books = booklogic.GetBooks(request.SubjectID, request.PressID.Value);
            if (books.IsHasData())
            {
                response.Books = books;
                response.ResponseCode = ResponseCode.Success;
                response.ResponseText = "请求成功";
                response.DataCount = books.Count;
            }
            else
            {
                response.Books = new List<BookDetailViewModel>();
                response.ResponseText = "暂无数据";
            }
            return response;
        }
    }
}
