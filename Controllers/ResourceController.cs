﻿using EduPlatform.Logic.System;
using EduPlatform.Manage.Infrastructure;
using EduPlatform.Manage.Models;
using EduPlatform.SearchModel;
using EduPlatform.SearchModel.Basal;
using EduPlatform.ServiceModel.Enums;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeacherClassRoomModel;
using TeacherClassRoomModel.ViewModel;

namespace EduPlatform.Manage.Controllers
{
    /// <summary>
    /// 资源相关
    /// </summary>
    public class ResourceController : BaseController
    {
        //
        // GET: /Resource/
        private readonly SubjectQuestionTypeLogic questionTypeLogic;
        private readonly ExamSyncQuestionTypeLogic examSyncQTLogic;
        private readonly QuestionBankLogic questionBankLogic;
        private readonly SubjectInfoLogic subjectLogic;
        private readonly TeacherClassRoomLogic.Imp.SubjectInfoLogic classroomsubjectLogic;
        private readonly GradeInfoLogic gradeLogic;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ResourceController()
        {
            questionTypeLogic = new SubjectQuestionTypeLogic();
            examSyncQTLogic = new ExamSyncQuestionTypeLogic();
            questionBankLogic = new QuestionBankLogic();
            subjectLogic = new SubjectInfoLogic();
            gradeLogic = new GradeInfoLogic();
            classroomsubjectLogic = new TeacherClassRoomLogic.Imp.SubjectInfoLogic();
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 题型列表
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult QuestionTypeList()
        {
            return View();
        }

        /// <summary>
        /// 题型查询
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult QuestionTypeListResult(QuestionTypeSearchModel search)
        {
            Page<QuestionTypeDetailInfo> pageResult;
            if(search.ExamType==(int)ExamType.Class)
            {
                pageResult = questionTypeLogic.QuestionTypeSearch(search);
            }
            else if (search.ExamType == (int)ExamType.Sync)
            {
                pageResult = examSyncQTLogic.QuestionTypeSearch(search);
            }
            else
            {
                pageResult = new Page<QuestionTypeDetailInfo>();
            }
            
            return View(pageResult);
        }

        /// <summary>
        /// 题库列表
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult QuestionBankList()
        {
            return View();
        }

        /// <summary>
        /// 题库查询
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult QuestionBankListResult(QuestionBankSearchModel search)
        {
            var pageResult = questionBankLogic.QuestionBankSearch(search);
            return View(pageResult);
        }

        /// <summary>
        /// 课程列表
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult SubjectList()
        {
            return View();
        }

        /// <summary>
        /// 课程查询
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubjectListResult(SubjectInfoSearchModel search)
        {
            var pageResult = classroomsubjectLogic.SubjectInfoSearch(search);
            return View(pageResult);
        }

        [HttpPost]
        public ActionResult SubjectJsonListResult(Guid gradeid)
        {
            var list = classroomsubjectLogic.SubjectInfoSearch(gradeid);
            return Json(list);
        }

        /// <summary>
        /// 学段列表
        /// </summary>
        /// <returns></returns>
        [AuthenUrlFilter]
        public ActionResult GradeList()
        {
            var grades = gradeLogic.GetGrades();
            return View(grades);
        }

        [HttpPost]
        public ActionResult SetSubjectDefault(Guid subjectid)
        {
            UpdateResponse rsp = new UpdateResponse();
            bool result = classroomsubjectLogic.SetSubjectDefault(subjectid) > 0;
            rsp.Success = result;
            return Json(rsp);
        }

        public ActionResult SubjectEdit(Guid subjectid)
        {
            var grades = gradeLogic.GetGrades();
            var subjectinfo = classroomsubjectLogic.GetModelByGuidId(subjectid);
            SubjectEditViewModel model = new SubjectEditViewModel();
            model.Grades = grades;
            model.SubjectInfo = subjectinfo;
            return View(model);
        }

        [HttpPost]
        public ActionResult SubjectEdit(Guid subjectid,string subjectname, Guid grade)
        {
            var subjectinfo = classroomsubjectLogic.GetModelByGuidId(subjectid);
            subjectinfo.Subject_Name = subjectname;
            subjectinfo.Subject_GradeID = grade;
            int result = classroomsubjectLogic.Update(subjectinfo);
            if (result > 0)
                return RedirectToAction("subjectlist", "resource");
            else
            {
                var grades = gradeLogic.GetGrades();
                SubjectEditViewModel model = new SubjectEditViewModel();
                model.Grades = grades;
                model.SubjectInfo = subjectinfo;
                ViewBag.Msg = "更新失败";
                return View(model);
            } 
        }
        public ActionResult SubjectAdd()
        { 
            var grades = gradeLogic.GetGrades();
            return View(grades);
        }

        [HttpPost]
        public ActionResult SubjectAdd(SubjectAddViewModel model)
        {
            if (ModelState.IsValid == true)
            {
                T_SubjectInfo subjectinfo = new T_SubjectInfo();
                subjectinfo.Subject_ID = Guid.NewGuid();
                subjectinfo.Subject_Name = model.SubjectName;
                subjectinfo.Subject_Note = model.SubjectName;
                subjectinfo.Subject_Code = model.SubjectCode;
                subjectinfo.Subject_GradeID = model.Grade;
                subjectinfo.Subject_State = 1;
                subjectinfo.Subject_Index = model.SubjectIndex;
                subjectinfo.Subject_AddDate = DateTime.Now;
                subjectinfo.IsDefault = 0;

                int result = classroomsubjectLogic.Insert(subjectinfo);

                return RedirectToAction("subjectlist", "resource");
            }
            else
            {
                var grades = gradeLogic.GetGrades();
                return View(grades);
            }
        }
    }
}
