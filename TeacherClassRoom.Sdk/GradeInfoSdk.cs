﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;

namespace TeacherClassRoom.Sdk
{
    public sealed class GradeInfoSdk:BaseSdk
    {
        public GradeInfoResponse GetGradeInfo(GradeInfoRequest request)
        {
            CheckBaseUrl();
            var rsp = new GradeInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<GradeInfoRequest, GradeInfoResponse>(TCRSckFactory.BaseUrl, "api/Grade/Get", request);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }
           
            return rsp;
        }
    }
}
