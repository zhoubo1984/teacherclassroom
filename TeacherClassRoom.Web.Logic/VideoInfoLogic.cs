﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;
using TeacherClassRoomModel.RequestModel;
using TeacherClassRoomModel.ResponseModel;

namespace TeacherClassRoom.Web.Logic
{
    public class VideoInfoLogic
    {
        VideoInfoSdk sdk = new VideoInfoSdk();

        public VideoInfoListResponse GetVideoList(VideoInfoListRequest request)
        {
            var rsp = sdk.GetVideoList(request);
            return rsp;
        }

        public VideoCollectResponse VideoCollect(VideoCollectRequest request)
        {
            var rsp = sdk.VideoCollect(request);

            return rsp;
        }

        public VideoInfoResponse VideoCollectDelete(VideoCollectDeleteRequest request)
        {
            var rsp = sdk.VideoCollectDelete(request);

            return rsp;
        }

        public VideoInfoResponse VideoRecommend(VideoRecommendRequest request)
        {
            var rsp = sdk.VideoRecommend(request);

            return rsp;
        }

        public VideoInfoListResponse GetCollectList(VideoCollectListRequest request)
        {
            var rsp = sdk.GetCollectList(request);

            return rsp;
        }

        public VideoResponse GetVideo(VideoInfoRequest request)
        {
            var rsp = sdk.GetVideo(request);

            return rsp;
        }

        public SubjectVideoCountResponse SubjectVideoCount(SubjectVideoCountRequest request)
        {
            var rsp = sdk.SubjectVideoCount(request);

            return rsp;
        }

        public VideoInfoResponse UpdatePlayCountVideo(VideoUpdatePlayCountRequest request)
        {
            var rsp = sdk.VideoUpdatePlayCount(request);

            return rsp;
        }
    }
}
