﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class AccountInfo
    {
        public Guid ID { get; set; }
        public int RowID { get; set; }
        [Required(ErrorMessage="必填项")]
        public string Name { get; set; }
        [Required(ErrorMessage = "必填项")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "必填项")]
        public string LoginPwd { get; set; }
        [Required(ErrorMessage = "必填项")]
        public string PwdConfirm { get; set; }
        [Required(ErrorMessage = "必填项")]
        public string NewPwd { get; set; }
        public int State { get; set; }
        public DateTime AddDate { get; set; }
    }
}