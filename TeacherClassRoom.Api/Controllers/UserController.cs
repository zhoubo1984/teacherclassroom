﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TeacherClassRoom.Api.Models;

using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;

namespace TeacherClassRoom.Api.Controllers
{
    public class UserController : ApiController
    {
        public static List<UserModel> allModeList = new List<UserModel>() {  
          new UserModel(){ Id=1,UserName="zhang", PassWord="123"}, 
          new UserModel(){ Id=2,UserName="lishi", PassWord="123456"}, 
          new UserModel(){ Id=3,UserName="wang", PassWord="1234567"} 
        };
        //Get  api/User/ 
        public IEnumerable<UserModel> GetAll()
        {
            //base.Request.Content.
            return allModeList;
        }
        //Get api/User/1 
        public IEnumerable<UserModel> GetOne(int id)
        {
            return allModeList.FindAll((m) => { return m.Id == id; });
        }
        //POST api/User/ 
        public bool PostNew(UserModel user)
        {
            try
            {
                allModeList.Add(user);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //Delete api/User/ 
        public int DeleteAll()
        {
            return allModeList.RemoveAll((mode) => { return true; });
        }
        //Delete api/User/1 
        public int DeleteOne(int id)
        {
            return allModeList.RemoveAll((m) => { return m.Id == id; });
        }
        //Put api/User  
        public int PutOne(int id, UserModel user)
        {
            List<UserModel> upDataList = allModeList.FindAll((mode) => { return mode.Id == id; });
            foreach (var mode in upDataList)
            {
                mode.PassWord = user.PassWord;
                mode.UserName = user.UserName;
            }
            return upDataList.Count;
        }

        [AcceptVerbs("POST")]
        public string SubmitTitle()
        {
            if(!System.Net.Http.HttpContentMultipartExtensions.IsMimeMultipartContent(Request.Content))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            HttpPostedFile file = HttpContext.Current.Request.Files[0];
            //var path = HttpContext.Current.Server.MapPath("~/File");
            //// 设置上传目录 
            //var provider = new MultipartFormDataStreamProvider(path);
            //var bodyparts = Request.Content.ReadAsMultipartAsync(provider).Result;
            //var username = bodyparts.FormData["username"];
            //var file = provider.FileData[0];//provider.FormData  
            //string orfilename = file.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
            //FileInfo fileinfo = new FileInfo(file.LocalFileName);
            //String ymd = DateTime.Now.ToString("yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            //String newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            //string fileExt = orfilename.Substring(orfilename.LastIndexOf('.'));
            //fileinfo.CopyTo(Path.Combine(path, newFileName + fileExt), true);
            //fileinfo.Delete();  

            
            //string result = "";
            //// 获取表单数据 
            //result += "formData userName: " + bodyparts.FormData["userName1"];
            //result += "<br />";
            //// 获取文件数据 
            //result += "fileData headers: " + bodyparts.FileData[0].Headers; // 上传文件相关的头信息 
            //result += "<br />";
            //result += "fileData localFileName: " + bodyparts.FileData[0].LocalFileName; // 文件在服务端的保存地址，需要的话自行 rename 或 move 
            return ""; 
        }
        public UserModel GetUserByName(string userName)
        {
            return allModeList.Find((m) => { return m.UserName.Equals(userName); });
        } 
    }
}
