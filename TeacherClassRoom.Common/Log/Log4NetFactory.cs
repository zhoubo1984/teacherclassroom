﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Log
{
    public class Log4NetFactory:ILogFactory
    {
        private string _configFileName;
        public Log4NetFactory(string configFileName)
        {
            _configFileName = configFileName;
        }

        public ILog Create()
        {
            var log = new Log4Net(_configFileName);
            log.LogConfig(_configFileName);
            return log;
        }
    }
}
