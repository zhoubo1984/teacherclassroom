﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EduPlatform.Manage.Infrastructure
{
    public static class AceUIHelper
    {
        public static HtmlString ShowTopNavigate(this HtmlHelper htmlHelper, NavigateInfo navigate)
        {
            StringBuilder navString = new StringBuilder();
            navString.Append("<ul class=\"breadcrumb\">");
            foreach(var nav in navigate.Parent)
            {
                navString.Append("<li>");
                if(nav.IsHome)
                {
                    navString.Append("<i class=\"icon-home home-icon\"></i>");
                }
                else
                {
                    navString.Append("<i></i>");
                }
                navString.AppendFormat("<a href=\"{0}\">{1}</a>",nav.Href,nav.Name);
                navString.Append("</li>");
            }

            navString.AppendFormat("<li class=\"active\">{0}</li>",navigate.Name);
            return new HtmlString(navString.ToString());
        }

        public static HtmlString ShowListNavigate(this HtmlHelper htmlHelper)
        {
            return null;
        }

        public static HtmlString ShowDropdownlist(IEnumerable<SelectListItem> options,string selectItem)
        {

            return null;
        }
    }
}