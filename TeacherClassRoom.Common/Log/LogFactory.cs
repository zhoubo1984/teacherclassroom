﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Common.Log
{
    public class LogFactory
    {
        static ILogFactory _currentLogFactory = null;

        public static void SetCurrent(ILogFactory logFactory)
        {
            _currentLogFactory = logFactory;
        }

        public static ILog CreateLog()
        {
            return (_currentLogFactory != null) ? _currentLogFactory.Create() : null;
        }
    }
}
