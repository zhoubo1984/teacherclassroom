﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TeacherClassRoom.Common.Extends
{
        /// <summary>
        /// string 方法扩展
        /// </summary>
        public static class StringExtends
        {
            /// <summary>
            /// string转int
            /// </summary>
            /// <param name="str"></param>
            /// <returns></returns>
            public static int ToInt32(this string str)
            {
                return Convert.ToInt32(str);
            }

            /// <summary>
            /// 字符串转guid
            /// </summary>
            /// <param name="str"></param>
            /// <returns></returns>
            public static Guid ToGuid(this string str)
            {
                return Guid.Parse(str);
            }

            /// <summary>
            /// 字符串转数字拼接字符串
            /// </summary>
            /// <param name="source"></param>
            /// <param name="split"></param>
            /// <param name="join"></param>
            /// <returns></returns>
            public static string JoinInt(this string source, char split, string join)
            {
                string s = string.Join(join, source.Split(split).Select(u =>
                {
                    return u.ToInt32();
                }));
                return s;
            }


            /// <summary>
            /// Guid格式拼接字符串
            /// </summary>
            /// <param name="source"></param>
            /// <param name="split"></param>
            /// <param name="join"></param>
            /// <returns></returns>
            public static string JoinString(this string source, char split, string join)
            {
                return string.Join(join, source.Split(split).Select(u =>
                {
                    return u.Trim().ToGuid();
                }));
            }

            /// <summary>
            /// 拼接Guid字符串
            /// </summary>
            /// <param name="source"></param>
            /// <param name="split"></param>
            /// <param name="join"></param>
            /// <returns></returns>
            public static string JoinGuidString(this string source, char split = '|', string join = "','")
            {
                return string.Join(join, source.Split(split).Select(u =>
                {
                    return u.Trim().ToGuid();
                }));
            }

            /// <summary>
            /// 获取文件扩展名
            /// </summary>
            /// <param name="fileName">文件名</param>
            /// <returns></returns>
            public static string GetFileExt(this string fileName)
            {
                if (fileName.LastIndexOf(".", StringComparison.CurrentCulture) == -1)
                {
                    return "";
                }

                return fileName.Substring(fileName.LastIndexOf(".", StringComparison.CurrentCulture) + 1, fileName.Length - fileName.LastIndexOf(".", StringComparison.CurrentCulture) - 1).ToLower();
            }

            /// <summary>
            /// 获取文件名，包含文件扩展
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string GetSingleFileName(this string filePath)
            {
                return filePath.Substring(filePath.LastIndexOf("\\", StringComparison.CurrentCulture) + 1, filePath.Length - filePath.LastIndexOf("\\", StringComparison.CurrentCulture) - 1);
            }

            /// <summary>
            /// 获取文件名,包含文件路径，不包含文件扩展
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string GetFullPathFileName(this string filePath)
            {
                //int pos = filePath.LastIndexOf(".");
                if (filePath.LastIndexOf(".", StringComparison.CurrentCulture) == -1)
                {
                    return filePath;
                }
                return filePath.Substring(0, filePath.LastIndexOf(".", StringComparison.CurrentCulture));
            }

            /// <summary>
            /// 截取字符串
            /// </summary>
            /// <param name="source"></param>
            /// <param name="len"></param>
            /// <returns></returns>
            public static string SubString(this string source, int len)
            {
                if (source.Length <= len)
                    return source;
                return source.Substring(0, len) + "...";
            }

            /// <summary>
            /// 获取文件的存放路径
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string GetFileSavePath(this string filePath)
            {
                return filePath.Substring(0, filePath.LastIndexOf("\\", StringComparison.CurrentCulture));
            }

            /// <summary>
            /// 获取单独的文件名，没有文件夹也没有文件扩展名
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string GetFileSingNameWithoutExt(this string filePath)
            {
                return filePath.Substring(filePath.LastIndexOf("\\", StringComparison.CurrentCulture) + 1, filePath.LastIndexOf(".", StringComparison.CurrentCulture) - filePath.LastIndexOf("\\", StringComparison.CurrentCulture) - 1);
            }

            /// <summary>
            /// 过滤sql参数字符串
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            public static string SqlSafeFilter(this string source)
            {
                source = source.Replace("'", "");
                source = source.Replace("\"", "");
                source = source.Replace("&", "&amp");
                source = source.Replace("<", "&lt");
                source = source.Replace(">", "&gt");

                source = source.Replace("delete", "");
                source = source.Replace("update", "");
                source = source.Replace("insert", "");
                source = source.Replace("truncate", "");
                source = source.Replace("exec", "");
                return source;
            }

            /// <summary>    
            /// 删除SQL注入特殊字符    
            /// 解然 20070622加入对输入参数sql为Null的判断    
            /// </summary>    
            public static string StripSqlInjection(this string sql)
            {
                if (!string.IsNullOrEmpty(sql))
                {
                    //过滤 ' --    
                    string pattern1 = @"(\%27)|(\')|(\-\-)";

                    //防止执行 ' or    
                    string pattern2 = @"((\%27)|(\'))\s*((\%6F)|o|(\%4F))((\%72)|r|(\%52))";

                    //防止执行sql server 内部存储过程或扩展存储过程    
                    string pattern3 = @"\s+exec(\s|\+)+(s|x)p\w+";

                    sql = Regex.Replace(sql, pattern1, string.Empty, RegexOptions.IgnoreCase);
                    sql = Regex.Replace(sql, pattern2, string.Empty, RegexOptions.IgnoreCase);
                    sql = Regex.Replace(sql, pattern3, string.Empty, RegexOptions.IgnoreCase);
                }
                return sql;
            }

            /// <summary>
            /// 获取Base64编码
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            public static string GetUtf8Base64String(this string source)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(source);
                string str = Convert.ToBase64String(bytes);
                return str;
            }
        }
}
