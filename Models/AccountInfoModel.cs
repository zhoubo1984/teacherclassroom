﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class AccountInfoModel
    {
        public string LoginName { set; get; }
        public string AccountID { set; get; }
        public int IsAdmin { get; set; }
    }
}