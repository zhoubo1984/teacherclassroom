﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    /// <summary>
    /// 客户端版本添加信息
    /// </summary>
    public class ClientVersionAddInfo
    {
        /// <summary>
        /// 客户端类型
        /// </summary>
        public int ClientType { set; get; }

        /// <summary>
        /// 升级编号
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public int UpgradeID { set; get; }

        /// <summary>
        /// 客户端版本号
        /// </summary>
        [Required(ErrorMessage="必填")]
        public string ClientVersion { set; get; }

        /// <summary>
        /// 发版说明
        /// </summary>
        public string ClientNote { set; get; }

        ///// <summary>
        ///// 下载链接
        ///// </summary>
        // [Required(ErrorMessage = "必填")]
        //public string DownloadUrl { set; get; }

        /// <summary>
        /// 文件名
        /// </summary>
         [Required(ErrorMessage = "必填")]
        public string FileName { set; get; }
    }
}