﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;

namespace TeacherClassRoom.Sdk
{
    public class PressInfoSdk:BaseSdk
    {
        public BookPressResponse GetPressInfo(BookPressRequest request)
        {
            CheckBaseUrl();
            var rsp = new BookPressResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<BookPressRequest, BookPressResponse>(TCRSckFactory.BaseUrl, "api/pressinfo/get", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }
    }
}
