﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeacherClassRoom.Web.Helpers
{
    public static class JsonExtends
    {
        public static string ToJsonStr<T>(this T obj)
            where T : class,new()
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T JsonToModel<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}