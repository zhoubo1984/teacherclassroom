﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;
using TeacherClassRoomModel.ResponseModel;
using TeacherClassRoomModel.RequestModel;

namespace TeacherClassRoom.Sdk
{
    public class VideoInfoSdk:BaseSdk
    {
        public VideoInfoListResponse GetVideoList(VideoInfoListRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoInfoListResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoInfoListRequest, VideoInfoListResponse>(TCRSckFactory.BaseUrl, "api/video/videoinfolist", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public SubjectVideoCountResponse SubjectVideoCount(SubjectVideoCountRequest request)
        {
            CheckBaseUrl();
            var rsp = new SubjectVideoCountResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<SubjectVideoCountRequest, SubjectVideoCountResponse>(TCRSckFactory.BaseUrl, "api/video/subjectvideocount", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public VideoResponse GetVideo(VideoInfoRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoInfoRequest, VideoResponse>(TCRSckFactory.BaseUrl, "api/video/VideoInfo", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public VideoCollectResponse VideoCollect(VideoCollectRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoCollectResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoCollectRequest, VideoCollectResponse>(TCRSckFactory.BaseUrl, "api/video/collect", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public VideoInfoResponse VideoCollectDelete(VideoCollectDeleteRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoCollectDeleteRequest, VideoInfoResponse>(TCRSckFactory.BaseUrl, "api/video/videocollectdelete", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public VideoInfoResponse VideoRecommend(VideoRecommendRequest request)
        {
            CheckBaseUrl();

            var rsp = new VideoInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoRecommendRequest, VideoInfoResponse>(TCRSckFactory.BaseUrl, "api/video/recommend", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

        public VideoInfoListResponse GetCollectList(VideoCollectListRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoInfoListResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoCollectListRequest, VideoInfoListResponse>(TCRSckFactory.BaseUrl, "api/video/videocollectedlist", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;

        }

        public VideoInfoResponse VideoUpdatePlayCount(VideoUpdatePlayCountRequest request)
        {
            CheckBaseUrl();
            var rsp = new VideoInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<VideoUpdatePlayCountRequest, VideoInfoResponse>(TCRSckFactory.BaseUrl, "api/video/videoupdateplaycount", request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }

    }
}
