﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using EduPlatform.Library;
using System.Web.Security;
using EduPlatform.Manage.Models;
using Newtonsoft.Json;
using EduPlatform.Manage.Infrastructure;

namespace EduPlatform.Manage
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            LogHelper.LogConfig(Server.MapPath(@"~\App_Data\log4net.config"));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            //LogHelper.Error(Utility.GetClientIP());
            LogHelper.Error(ex);
            var httpStatusCode = (ex is HttpException) ? (ex as HttpException).GetHttpCode() : 500;
            //RedirectUrl("/home/page404");
            
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                AccountInfoModel accountInfo = JsonConvert.DeserializeObject<AccountInfoModel>(authTicket.UserData);
                AccountInfoPrincipal newUser = new AccountInfoPrincipal(accountInfo.LoginName);
                newUser.AccountId = accountInfo.AccountID;
                newUser.AccountName = accountInfo.LoginName;
                newUser.IsAdmin = accountInfo.IsAdmin;

                HttpContext.Current.User = newUser;
            }
        }

        private void RedirectUrl(string url)
        {
            Response.Clear();
            Response.BufferOutput = true;
            Response.Redirect(url, true);
        }
    }
}