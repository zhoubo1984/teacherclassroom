﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Infrastructure
{
    public class NavigateInfo
    {
        public string Href { set; get; }
        public string Name { set; get; }
        public bool IsHome { set; get; }

        public List<NavigateInfo> Parent { set; get; }

        //public bool IsOpen { set; get; }

        //public bool IsActive { set; get; }
    }
}