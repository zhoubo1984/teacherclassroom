﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;

namespace TeacherClassRoom.Web.Logic
{
    public class SubGradeLogic
    {
        public SubGradeInfoResponse GetAllSubjectGradeInfo()
        {
            SubGradeInfoSdk sdk = new SubGradeInfoSdk();
            var rsp = sdk.GetAllSubjectGradeInfo();

            return rsp;
        }
    }
}
