﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeacherClassRoom.Api.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string PassWord { get; set; }
    }
}