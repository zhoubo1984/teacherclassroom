﻿using EduPlatform.Manage.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace EduPlatform.Manage.Controllers
{
    [AccountAuthorize]
    public class BaseController : Controller
    {
        protected AccountInfoPrincipal account = System.Web.HttpContext.Current.User as AccountInfoPrincipal;
        

        public BaseController()
        {
           
        }
    }
}
