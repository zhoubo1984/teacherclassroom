﻿using EduPlatform.Logic.System;
using EduPlatform.Framework;
using EduPlatform.ServiceModel;
using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using EduPlatform.ServiceModel.Resource;


namespace TeacherClassRoom.Api.Controllers
{
    public class KonwledgeInfoController : ApiController
    {
        //
        // GET: /KonwledgeInfo/

        [HttpPost]
        public KnowledgeSearchResponse Get(KnowledgeSearchRequest request)
        {
            var response = new KnowledgeSearchResponse();
            try
            {
                TeacherClassRoomLogic.Imp.KnowledgeLogic logic = new TeacherClassRoomLogic.Imp.KnowledgeLogic();
                var knowledges = (from u in logic.GetKnowledgesBySubjectId(request.SubjectID)
                                 select new KnowledgeInfoViewModel
                                 {
                                     Index = u.KG_Index,
                                     KnowledgeID = u.KG_ID,
                                     KnowledgeValue = u.KG_Name,
                                     Path = u.KG_Path,
                                     PID = u.KG_PID == null ? "" : u.KG_PID.ToString(),
                                     TrueValue =u.KG_Value
                                 }).ToList();
                if(knowledges.IsHasData())
                {
                    response.Knowledges = knowledges;
                    response.ResponseText = "请求成功";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = knowledges.Count;
                }
                else
                {
                    response.Knowledges = new List<KnowledgeInfoViewModel>();
                    response.ResponseText = "暂无数据";
                    response.ResponseCode = ResponseCode.Success;
                    response.DataCount = 0;
                }
            }
            catch(Exception ex)
            {
                response.Knowledges = new List<KnowledgeInfoViewModel>();
                response.ResponseText = "获取失败";
                response.ResponseCode = ResponseCode.InernalError;
                response.DataCount = 0;
            }
            
            return response;
        }
    }
}
