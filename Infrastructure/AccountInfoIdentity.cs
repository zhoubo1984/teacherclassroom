﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace EduPlatform.Manage.Infrastructure
{
    public class AccountInfoIdentity : IIdentity
    {
        private FormsAuthenticationTicket ticket;
        private HttpContext context = HttpContext.Current;

        public AccountInfoIdentity(FormsAuthenticationTicket ticket)
        {
            this.ticket = ticket;
        }

        private string userId;
        private string loginName;
        private int isAdmin;

        public AccountInfoIdentity(string userid, string loginName, int isAdmin)
        {
            this.userId = userid;
            this.loginName = loginName;
            this.isAdmin = isAdmin;
        }


        public string UserID
        {
            get
            {
                return this.userId;
            }
        }
        public string UserName
        {
            get
            {
                return this.loginName;
            }
        }
        public int IsAdmin
        {
            get
            {
                return this.isAdmin;
            }
        }

        public string AuthenticationType
        {
            get
            {
                return "Forms";
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }

        public string Name
        {
            get
            {
                return this.loginName;
            }
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2}", UserID, UserName, IsAdmin);
        }
    }
}