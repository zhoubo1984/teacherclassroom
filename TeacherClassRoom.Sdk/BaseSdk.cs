﻿using EduPlatform.ServiceModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherClassRoom.Sdk
{
    public class BaseSdk
    {
        internal void CheckBaseUrl()
        {
            if (string.IsNullOrEmpty(TCRSckFactory.BaseUrl))
            {
                throw new ArgumentException("未初始化远程api基地址");
            }
        }

        internal void CreateErrorResponse<T>(T response,IRestResponse<T> ir)
            where T:BaseResponse
        {
            if (ir.ErrorException != null)
            {
                response.DataCount = 0;
                response.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                response.ResponseText = ir.ErrorMessage;
            }
            else
            {
                response = ir.Data;
            }
        }
    }
}
