﻿using EduPlatform.Domain.System;
using EduPlatform.Logic.System;
using EduPlatform.Manage.Models;
using EduPlatform.SearchModel;
using EduPlatform.SearchModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduPlatform.Framework;
using EduPlatform.ServiceModel;
using EduPlatform.Manage.Infrastructure;
using TeacherClassRoomModel.ViewModel;
using TeacherClassRoomLogic.Imp;
using TeacherClassRoom.Common;

namespace EduPlatform.Manage.Controllers
{
    public class UnitController : BaseController
    {
        //
        // GET: /Unit/
        private UnitInfoLogic unitLogic = new UnitInfoLogic();
        private UnitLogic classroomunitLogic = new UnitLogic();

        private EduPlatform.Logic.System.GradeInfoLogic gradeLogic = new EduPlatform.Logic.System.GradeInfoLogic();

        public ActionResult Index()
        {
            return View();
        }

        [AuthenUrlFilter]
        public ActionResult UnitList(Guid? unitid)
        {
            ViewBag.PUid = unitid;
            var grades = gradeLogic.GetGrades();
            return View(grades);
        }

        [HttpPost]
        public ActionResult ImportUnit(UnitImportViewModel model, HttpPostedFileBase file)
        {
            var response = new BaseResponse();
            if (ModelState.IsValid && file != null)
            {
                //检查文件
                if (file.ContentLength > 4 * 1024 * 1024)
                {

                    response.DataCount = 0;
                    response.ResponseCode = ResponseCode.ParamError;
                    response.ResponseText = "文件上传不得大于4M";

                    return Json(response);
                }
                //保存数据
                byte[] filecontent = file.InputStream.ToByteArray();
                FileManager fm = new FileManager();
                string tmpfilepath = fm.AddTempFile(Server.MapPath("~/TempFiles"), filecontent);
                classroomunitLogic.ImportUnit(model, tmpfilepath);

                response.DataCount = 1;
                response.ResponseCode = ResponseCode.Success;
                response.ResponseText = "导入成功！";

                return Json(response);
            }
            else
            {
                response.DataCount = 0;
                response.ResponseCode = ResponseCode.ParamError;
                response.ResponseText = "请求错误，请检查输入";

                return Json(response);
            }
        }

        [HttpPost]
        public ActionResult UnitResultList(UnitInfoSearchModel search)
        {
            var pageResult = classroomunitLogic.UnitInfoSearch(search);
            return View(pageResult);
        }

        private UnitEditViewModel Createeditvm(Guid? unitid)
        {
            TeacherClassRoomLogic.Imp.AccountInfoLogic accountinfologic = new TeacherClassRoomLogic.Imp.AccountInfoLogic();
            
            UnitEditViewModel vm = new UnitEditViewModel();
            var grades = gradeLogic.GetGrades();
            TeacherClassRoomModel.T_UnitInfo unitinfo = null;
            TeacherClassRoomModel.T_AccountInfo relateaccountinfo = null;
            if(unitid!=null)
            {
                unitinfo = classroomunitLogic.GetModelByGuidId(unitid.Value);
            }
            if (unitinfo!=null&&unitinfo.Unit_AccountID != null)
            {
                relateaccountinfo = accountinfologic.GetModelByGuidId(unitinfo.Unit_AccountID.Value);
            }
            vm.Grades = grades;
            vm.UnitInfo = unitinfo;
            vm.AccountInfo = relateaccountinfo;

            return vm;
        }

        public ActionResult EditUnit(Guid unitid)
        {
            return View(Createeditvm(unitid));
        }

        //修改单位信息
        [HttpPost]
        public ActionResult EditUnit(UnitAddModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Msg = "添加失败，请检查输入";
                return View(Createeditvm(model.UnitId));
            }

            if (model.UnitId == default(Guid))
            {
                ViewBag.Msg = "单位编号错误";
                return View(Createeditvm(model.UnitId));
            }

            if (!model.Grade.IsHasData())
            {
                ViewBag.Msg = "学段为空，请检查输入";
                return View(Createeditvm(model.UnitId));
            }

            AreaInfoLogic areaLogic = new AreaInfoLogic();
            string cityName = "";
            var areaInfo = areaLogic.GetAreaInfo(model.City);
            if (areaInfo != null)
            {
                cityName = areaInfo.Area_Name;
            }

            string provinceName = "";
            var provinceInfo = areaLogic.GetAreaInfo(model.Province);
            if (provinceInfo != null)
            {
                provinceName = provinceInfo.Area_Name;
            }

            string gradestring = string.Join(",", model.Grade);
            string path = "";
            var unitinfo = classroomunitLogic.GetModelByGuidId(model.UnitId);
            //var unitinfo = unitLogic.GetModelByGuidId(model.UnitId);
            unitinfo.Unit_ProvinceID = model.Province;
            unitinfo.Unit_ProvinceName = provinceName;
            unitinfo.Unit_CityID = model.City;
            unitinfo.Unit_CityName = cityName;
            unitinfo.Unit_Grade = gradestring;
            unitinfo.Unit_Name = model.UnitName;

            if (!string.IsNullOrEmpty(model.ParentUnit))
            {
                var parentUnit = unitLogic.GetModelByGuidId(model.ParentUnit.ToGuid());
                path = parentUnit.Unit_Path;
                unitinfo.Unit_Path = string.Format("{0}_{1}", path, unitinfo.Unit_ID);
                unitinfo.Unit_PID = model.ParentUnit.ToGuid();
            }
            else
            {
                unitinfo.Unit_Path = string.Format("{0}", unitinfo.Unit_ID);
            }
            //更新单位信息
            int updateResult = classroomunitLogic.Update(unitinfo);
            
            //bool updateResult = unitLogic.Update(unitinfo) > 0;

            TeacherClassRoomLogic.Imp.AccountInfoLogic accountlogic = new TeacherClassRoomLogic.Imp.AccountInfoLogic();
            var accountinfo = accountlogic.GetModelByGuidId(unitinfo.Unit_AccountID.Value);
            accountinfo.Account_Name = model.AccountName;
            accountinfo.Account_LoginName = model.AccountLoginName;
            bool accountupdateResult = accountlogic.Update(accountinfo)>0;

            return RedirectToAction("unitlist", "unit", null);
        }

        public ActionResult AddUnit(Guid? puid)
        {
            return View(Createeditvm(puid));
        }

        /// <summary>
        /// 添加单位信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUnit(UnitAddModel model)
        {
            var parentuid = !string.IsNullOrEmpty(Request["puid"]) ? Request["puid"] : model.ParentUnit;
            Guid? pguid = null;
            if(!string.IsNullOrEmpty(parentuid)) pguid=Guid.Parse(parentuid);
            var vm = Createeditvm(pguid);
            if (!ModelState.IsValid)
            {
                ViewBag.Msg = "添加失败，请检查输入";
                return View(vm);
            }

            //if (model.UnitId == default(Guid))
            //{
            //    ViewBag.Msg = "单位编号错误";
            //    return View(vm);
            //}

            if (!model.Grade.IsHasData())
            {
                ViewBag.Msg = "学段为空，请检查输入";
                return View(vm);
            }

            var item = unitLogic.GetModelByGuidId(model.UnitId);
            if (item != null)
            {
                ViewBag.Msg = "单位编号已存在";
                return View(vm);
            }

            AreaInfoLogic areaLogic = new AreaInfoLogic();
            string cityName = "";
            var areaInfo = areaLogic.GetAreaInfo(model.City);
            if (areaInfo != null)
            {
                cityName = areaInfo.Area_Name;
            }

            string provinceName = "";
            var provinceInfo = areaLogic.GetAreaInfo(model.Province);
            if (provinceInfo != null)
            {
                provinceName = provinceInfo.Area_Name;
            }

            string gradestring = string.Join(",", model.Grade);
            string path = "";


            TeacherClassRoomModel.T_UnitInfo unit = new TeacherClassRoomModel.T_UnitInfo()
            {
                Unit_Name = model.UnitName,
                Unit_ID = Guid.NewGuid(),
                Unit_AddDate = DateTime.Now,
                Unit_CityID = model.City,
                Unit_ProvinceID = model.Province,
                Unit_UnitType = 1,
                Unit_State = 1,
                Unit_Index = 0,
                Unit_ProvinceName = provinceName,
                Unit_CityName = cityName,
                Unit_Grade = gradestring
            };
            if (string.IsNullOrEmpty(parentuid))
                unit.Unit_PID = null;
            else
                unit.Unit_PID = Guid.Parse(parentuid);
            if (!string.IsNullOrEmpty(model.ParentUnit))
            {
                var parentUnit = unitLogic.GetModelByGuidId(model.ParentUnit.ToGuid());
                path = parentUnit.Unit_Path;
                unit.Unit_Path = string.Format("{0}_{1}", path, unit.Unit_ID);
                unit.Unit_PID = model.ParentUnit.ToGuid();
            }
            else
            {
                unit.Unit_Path = string.Format("{0}", unit.Unit_ID);
            }

            T_AccountInfo accountinfo = new T_AccountInfo();
            accountinfo.Account_AddDate = DateTime.Now;
            accountinfo.Account_ID = Guid.NewGuid();
            accountinfo.Account_IsAdmin = 0;
            accountinfo.Account_LoginName = unit.Unit_Name;
            accountinfo.Account_LoginPwd = Md5Helper.GetMd5Hash("1");
            accountinfo.Account_Name = unit.Unit_Name;
            accountinfo.Account_State = 1;

            unit.Unit_AccountID = accountinfo.Account_ID;
            //1.添加学校
            bool updateResult = classroomunitLogic.Insert(unit) > 0;

            //2.添加该校管理员
            EduPlatform.Logic.System.AccountInfoLogic logic = new EduPlatform.Logic.System.AccountInfoLogic();
            logic.Insert(accountinfo);
           
            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            return View(vm);
        }

        /// <summary>
        /// 查询单位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUnits(string id, string name, string level,string selectedgui="")
        {
            var units = unitLogic.GetUnits(UnitInfoType.EduUnit);
            var unitsModel = units.Select(u =>
            {
                return new
                {
                    id = u.Unit_ID.ToString(),
                    name = u.Unit_Name,
                    pId = u.Unit_PID == null ? "" : u.Unit_PID.ToString(),
                    @checked = u.Unit_ID.ToString().ToLower()==selectedgui.ToLower()
                };
            });
            return Json(unitsModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除单位信息
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UnitDelete(Guid unitId,int state)
        {
            var rsp = new UpdateResponse();
            var unit = classroomunitLogic.GetModelByGuidId(unitId);

            if (unit == null)
            {
                rsp.Success = false;
                rsp.Message = "单位信息不存在";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            if (unit.Unit_UnitType == (int)UnitInfoType.System&&state==0)
            {
                rsp.Success = false;
                rsp.Message = "系统单位不允许禁用";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            var subUnits = unitLogic.GetSubUnitInfos(unitId);
            if (subUnits.IsHasData()&&state==0)
            {
                rsp.Success = false;
                rsp.Message = "本单位下有下级单位，不允许禁用!";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            UserInfoLogic userInfoLogic = new UserInfoLogic();
            var userCount = userInfoLogic.GetUserByUnit(unitId);

            if (userCount > 0&&state==0)
            {
                rsp.Success = false;
                rsp.Message = "本单位下已有用户信息，不允许禁用!";
                return Json(rsp, JsonRequestBehavior.AllowGet);
            }

            bool result = classroomunitLogic.Delete(unit)>0;

            //unit.Unit_State = state;
            //rsp.Success = classroomunitLogic.Update(unit);
            rsp.Success = result;
            rsp.Message = rsp.Success ? "操作成功" : "操作失败";
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
    }
}
