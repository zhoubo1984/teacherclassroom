﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;

namespace TeacherClassRoom.Web.Logic
{
    public class GradeInfoLogic
    {
        public GradeInfoResponse GetGradeInfo(GradeInfoRequest request)
        {
            GradeInfoSdk sdk = new GradeInfoSdk();
            var rsp = sdk.GetGradeInfo(request);
            
            return rsp;
        }
    }
}
