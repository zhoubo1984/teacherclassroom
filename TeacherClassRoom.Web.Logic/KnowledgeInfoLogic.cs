﻿using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;

namespace TeacherClassRoom.Web.Logic
{
    public class KnowledgeInfoLogic
    {
        public KnowledgeSearchResponse GetKnowledgeInfo(KnowledgeSearchRequest request)
        {
            KnowledgeInfoSdk sdk = new KnowledgeInfoSdk();
            var rsp = sdk.GetKnowledgeInfo(request);

            return rsp;
        }
    }
}
