﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EduPlatform.ServiceModel.Basal;
using TeacherClassRoomModel.RequestModel;
using TeacherClassRoom.Common.ApiClient;

namespace TeacherClassRoom.Sdk
{
    public class SubjectInfoSdk:BaseSdk
    {
        public SubjectInfoResponse GetSubjectInfo(SubjectGradeRequest request)
        {
            CheckBaseUrl();
            var rsp = new SubjectInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<SubjectGradeRequest, SubjectInfoResponse>(TCRSckFactory.BaseUrl, "api/subjectinfo/get", request);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = ex.Message;
            }
            return rsp;
        }

        public SubjectInfoResponse GetAllSubjectInfo()
        {
            CheckBaseUrl();
            var rsp = new SubjectInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<SubjectGradeRequest, SubjectInfoResponse>(TCRSckFactory.BaseUrl, "api/subjectinfo/getall", null);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = ex.Message;
            }
            return rsp;
        }
    }
}
