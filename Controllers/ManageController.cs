﻿using EduPlatform.Logic.System;
using EduPlatform.Manage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduPlatform.Framework;

namespace EduPlatform.Manage.Controllers
{
    public class ManageController : BaseController
    {
        //
        // GET: /Manage/
        SystemActionMenuLogic menuLogic = new SystemActionMenuLogic();
        AuthGroupMenuLogic groupAuthLogic = new AuthGroupMenuLogic();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Menus(string url)
        {
            List<string> userMenus = groupAuthLogic.GetMeunsByUser(Guid.Parse(account.AccountId))
                .Select(t=>t.AuthMenu_MenuID).ToList();
            string userMenu = string.Join("|", userMenus);
            var menus = userMenu.Split('|').Distinct();
            var menusDomain = menuLogic.GetMenus();
            var menusDto = menusDomain.Select(u =>
            {
                return new SystemMenuInfo()
                {
                    Icon = u.M_ICON,
                    MenuID = u.M_ID,
                    MenuName = u.M_ActionName,
                    Pid = u.M_PID,
                    Url = u.M_ActionUrl,
                    IsParent = u.M_IsParent == 1 ? true : false
                };
            });
            if (account.IsAdmin == 0)
                menusDto=menusDto.Where(u => menus.Contains(u.MenuID.ToString()));
            menusDto=menusDto.ToList();

            var rootMenus = menusDto.Where(u => u.IsParent).ToList();

            rootMenus.ForEach(u =>
            {
                u.SubMenus = menusDto.Where(u2 => u2.Pid == u.MenuID).ToList();
                var selectMenus = u.SubMenus.SingleOrDefault(m => m.Url.ToLower() == url.ToLower());
                if(selectMenus!=null)
                {
                    u.IsOpen = true;

                    u.SubMenus.ForEach(sm => { 
                      if(sm.Url.ToLower()==url.ToLower())
                      {
                          sm.IsActive = true;
                      }
                    });
                }


            });
            
            return View(rootMenus);
        }

    }
}
