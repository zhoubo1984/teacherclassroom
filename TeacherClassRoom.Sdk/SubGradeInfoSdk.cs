﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using TeacherClassRoom.Common.ApiClient;

namespace TeacherClassRoom.Sdk
{
    public class SubGradeInfoSdk : BaseSdk
    {
        public SubGradeInfoResponse GetAllSubjectGradeInfo()
        {
            CheckBaseUrl();
            var rsp = new SubGradeInfoResponse();
            try
            {
                rsp = ApiHelper.PostDataToServer<SubGradeInfoRequest, SubGradeInfoResponse>(TCRSckFactory.BaseUrl, "api/grade/getallsubjectgradeinfo", null);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = ex.Message;
            }

            return rsp;
        }
    }
}
