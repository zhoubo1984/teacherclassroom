﻿using EduPlatform.ServiceModel;
using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeacherClassRoomLogic.Imp;
using EduPlatform.Framework;

namespace TeacherClassRoom.Api.Controllers
{
    public class PressInfoController : ApiController
    {
        [HttpPost]
        public BookPressResponse Get(BookPressRequest request)
        {
            var response = new BookPressResponse();
            
            if(request==null||request.SubjectID==null)
            {
                response.Press = new List<PressViewModel>();
                response.ResponseCode = ResponseCode.ParamError;
                response.ResponseText = "请求参数无效";
                response.DataCount = 0;
                return response;
            }
            PressInfoLogic presslogic = new PressInfoLogic();
            var presses = presslogic.GetPublishs(request.SubjectID);
            if (presses.IsHasData())
            {
                response.Press = presses;
                response.ResponseCode = ResponseCode.Success;
                response.ResponseText = "请求成功";
                response.DataCount = response.Press.Count;
            }
            else
            {
                response.Press = new List<PressViewModel>();
                response.ResponseText = "暂无数据";
            }
            return response;
        } 
    }
}
