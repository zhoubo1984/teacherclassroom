﻿using EduPlatform.ServiceModel.Basal;
using EduPlatform.ServiceModel.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeacherClassRoom.Web.Helpers;
using TeacherClassRoom.Web.Logic;
using TeacherClassRoomModel.RequestModel;
using TeacherClassRoomModel.ResponseModel;
namespace TeacherClassRoom.Web.Controllers
{
    public class BasalApiController : Controller
    {
        //
        // GET: /Basal/
        [HttpPost]
        public ActionResult GradeList()
        {
            var rsp = new GradeInfoResponse();
            try
            {
                GradeInfoRequest request = new GradeInfoRequest();
                GradeInfoLogic gradelogic = new GradeInfoLogic();
                rsp = gradelogic.GetGradeInfo(request);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }
            
            return Json(rsp);
        }

        [HttpPost]
        public ActionResult SubjectList(string gradeid)
        {
            var rsp = new SubjectInfoResponse();

            try
            {
                Guid guid =Guid.Empty;
                if(!Guid.TryParse(gradeid,out guid))
                {
                    rsp.DataCount = 0;
                    rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                    rsp.ResponseText = "传入参数错误";
                }
                else
                {
                    SubjectGradeRequest request = new SubjectGradeRequest();
                    request.GradeId = Guid.Parse(gradeid);
                    SubjectInfoLogic subjectlogic = new SubjectInfoLogic();
                    rsp = subjectlogic.GetSubjectInfo(request);
                }
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        /// <summary>
        /// 获取所有学科
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubjectAllList()
        {
            var rsp = new SubjectInfoResponse();
            try
            {
                SubjectInfoLogic subjectlogic = new SubjectInfoLogic();
                rsp = subjectlogic.GetAllSubjectInfo();
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        /// <summary>
        /// 获取学科下的出版社
        /// </summary>
        /// <param name="subjectid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PressInfoList(string subjectid)
        {
            var rsp = new BookPressResponse();
            Guid guid = Guid.Empty;
            if (!Guid.TryParse(subjectid, out guid))
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                rsp.ResponseText = "传入参数错误";
            }
            else
            {
                try
                {
                    PressInfoLogic pressinfologic = new PressInfoLogic();
                    rsp = pressinfologic.GetPressInfo(new BookPressRequest() { SubjectID = guid });
                }
                catch (Exception ex)
                {
                    rsp.DataCount = 0;
                    rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                    rsp.ResponseText = "系统内部错误";
                }
            }

            return Json(rsp);
        }

        /// <summary>
        /// 获取所有年级信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubjectAllGradeList()
        {
            var rsp = new SubGradeInfoResponse();
            try
            {
                SubGradeLogic subjectlogic = new SubGradeLogic();
                rsp = subjectlogic.GetAllSubjectGradeInfo();
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        [HttpPost]
        public ActionResult BookInfoList(string subjectid,string pressid)
        {
            var rsp = new BookSearchResponse();

            try
            {
                Guid guid =Guid.Empty;
                if(!Guid.TryParse(pressid,out guid)||!Guid.TryParse(subjectid,out guid))
                {
                    rsp.DataCount = 0;
                    rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                    rsp.ResponseText = "传入参数错误";
                }
                else
                {
                    BookSearchRequest request = new BookSearchRequest();
                    request.PressID = Guid.Parse(pressid);
                    request.SubjectID = Guid.Parse(subjectid);
                    BookInfoLogic subjectlogic = new BookInfoLogic();
                    rsp = subjectlogic.GetBookInfo(request);
                }
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        [HttpPost]
        public ActionResult KnowledgeInfoList(string subjectid)
        {
            var rsp = new KnowledgeSearchResponse();
            try
            {
                Guid guid = Guid.Empty;
                if (!Guid.TryParse(subjectid, out guid))
                {
                    rsp.DataCount = 0;
                    rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.Fail;
                    rsp.ResponseText = "传入参数错误";
                }
                else
                {
                    KnowledgeSearchRequest request = new KnowledgeSearchRequest();
                    request.SubjectID = Guid.Parse(subjectid);
                    KnowledgeInfoLogic subjectlogic = new KnowledgeInfoLogic();
                    rsp = subjectlogic.GetKnowledgeInfo(request);
                    rsp.Knowledges = rsp.Knowledges.OrderBy(i => i.Index).ToList();

                    var knowledges = rsp.Knowledges.Select(t => new
                    {
                        id = t.KnowledgeID,
                        pId = t.PID.ToLower(),
                        name = t.KnowledgeValue.Length > 9 ? t.KnowledgeValue.Substring(0, 8) + "..." : t.KnowledgeValue,
                        click = true,
                        t = t.TrueValue,
                        //iconOpen = @"/Content/images/7.png",
                        //iconClose = @"/Content/images/6.png",
                        open = true
                    });

                    //var treejsonmodel = TreeJsonConverter.Convert(rsp.Knowledges);

                    return Json(new { DataCount = rsp.DataCount, Knowledges = knowledges, ResponseCode = rsp.ResponseCode, ResponseText = rsp.ResponseText });
                }
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        [HttpPost]
        public ActionResult VideoInfoList(VideoInfoListRequest model)
        {
            var rsp = new VideoInfoListResponse();
            try
            {
                model.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                VideoInfoLogic logic = new VideoInfoLogic();
                rsp = logic.GetVideoList(model);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return new JsonNetResult(rsp);
        }

        /// <summary>
        /// 根据视频ID获取视频详细信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VideoInfo(VideoInfoRequest model)
        {
            var rsp = new VideoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                rsp = logic.GetVideo(model);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return new JsonNetResult(rsp);
        }

        /// <summary>
        /// 加入收藏
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CollectVideo(VideoCollectRequest request)
        {
            var rsp = new VideoCollectResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                request.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                rsp = logic.VideoCollect(request);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        [HttpPost]
        public ActionResult VideoCollectDelete(VideoCollectDeleteRequest request)
        {
            var rsp = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                request.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                rsp = logic.VideoCollectDelete(request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        /// <summary>
        /// 推荐视频
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RecommendVideo(VideoRecommendRequest request)
        {
            var rsp = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                request.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                rsp = logic.VideoRecommend(request);
            }
            catch(Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return Json(rsp);
        }

        [HttpPost]
        public ActionResult VideoCollectedList(VideoCollectListRequest request)
        {
            var rsp = new VideoInfoListResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                request.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                rsp = logic.GetCollectList(request);

                //获取学科数量统计
                SubjectVideoCountRequest videorequest = new SubjectVideoCountRequest();
                videorequest.UserID = SubjectInfoHelper.GetInfo().UserId.Value;
                videorequest.VideoName = request.VideoName;
                var subjectvideocountrsp = logic.SubjectVideoCount(videorequest);

                rsp.SubjectVideoCount = subjectvideocountrsp.SubjectVideoCount;
                int totalcount = 0;
                subjectvideocountrsp.SubjectVideoCount.ForEach(item =>
                {
                    totalcount += item.VideoCount;
                    item.SubjectName = SubjectCodeToName.Convert(item.SubjectCode);
                });
                rsp.TotalSubjectVideoCount = totalcount;
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return new JsonNetResult(rsp);
        }

        [HttpPost]
        public ActionResult VideoUpdatePlayCount(VideoUpdatePlayCountRequest request)
        {
            var rsp = new VideoInfoResponse();
            try
            {
                VideoInfoLogic logic = new VideoInfoLogic();
                rsp = logic.UpdatePlayCountVideo(request);
            }
            catch (Exception ex)
            {
                rsp.DataCount = 0;
                rsp.ResponseCode = EduPlatform.ServiceModel.ResponseCode.InernalError;
                rsp.ResponseText = "系统内部错误";
            }

            return new JsonNetResult(rsp);
        }
    }
}
