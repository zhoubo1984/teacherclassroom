﻿using EduPlatform.Domain.System;
using EduPlatform.Logic.System;
using EduPlatform.Manage.Models;
using EduPlatform.SearchModel;
using EduPlatform.SearchModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduPlatform.Framework;
using EduPlatform.Manage.Infrastructure;

namespace EduPlatform.Manage.Controllers
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SysConfigController : BaseController
    {
        //
        // GET: /SysConfig/
        RouteTableConfigLogic routeTableLogic = new RouteTableConfigLogic();
        SystemConfigLogic systemConfigLogic = new SystemConfigLogic();
        ClientVersionInfoLogic clientVersionLogic = new ClientVersionInfoLogic();
        GradeInfoLogic gradeinfoLogic = new GradeInfoLogic();
        SubjectInfoLogic subjectinfoLogic = new SubjectInfoLogic();

        public ActionResult Index()
        {
            return View();
        }
        [AuthenUrlFilter]
        public ActionResult DataRouteList()
        {
            var dataRoutes = routeTableLogic.LoadRoutes();
            return View(dataRoutes);
        }

        [HttpPost]
        public ActionResult DataRouteResultList(DataRouteSearchModel search)
        {
            var pageResult = routeTableLogic.DataRouteSearch(search);
            return View(pageResult);
        }
        [AuthenUrlFilter]
        public ActionResult DataConfigList()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DataConfigListResult(SystemConfigSearchModel search)
        {
            var pageResult = systemConfigLogic.SystemConfigSearch(search);
            return View(pageResult);
        }


        [AuthenUrlFilter]
        public ActionResult DataRouteAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DataRouteAdd(DataRouteAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            RouteTableConfigLogic routeTableLogic = new RouteTableConfigLogic();
            var item = routeTableLogic.GetDataRoute(model.SubjectID.ToGuid());
            if (item != null)
            {
                ViewBag.Msg = "此课程数据库信息已存在!";
                return View();
            }

            T_RouteTableConfig routeTable = new T_RouteTableConfig()
            {
                RTC_AddDate = DateTime.Now,
                RTC_ConnString = model.ConnString,
                RTC_DatabaseName = model.Database,
                RTC_GradeID = model.GradeID.ToGuid(),
                RTC_ID = Guid.NewGuid(),
                RTC_State = 1,
                RTC_SubjectID = model.SubjectID.ToGuid()
            };


            bool updateResult = routeTableLogic.Insert(routeTable) > 0;
            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            return View();
        }
        [AuthenUrlFilter]
        public ActionResult DataConfigAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DataConfigAdd(DataConfigAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var item = systemConfigLogic.GetConfigByKey(model.ConfigKey);
            if (item != null)
            {
                ViewBag.Msg = "相关配置参数已存在";
                return View();
            }

            T_SystemConfig config = new T_SystemConfig()
            {
                SC_State = 1,
                SC_AddDate = DateTime.Now,
                SC_ConfigKey = model.ConfigKey,
                SC_ConfigValue = model.ConfigValue,
                SC_Note = model.ConfigNote,
                SC_ID = Guid.NewGuid()
            };

            bool updateReesult = systemConfigLogic.Insert(config) > 0;
            ViewBag.Msg = updateReesult ? "添加成功" : "添加失败";
            return View();
        }

        [HttpGet]
        public ActionResult DataConfigEdit(Guid configId)
        {
            var config = systemConfigLogic.GetModelByGuidId(configId);
            var configModel = new DataConfigEditModel()
            {
                ConfigId = config.SC_ID.ToString(),
                ConfigKey = config.SC_ConfigKey,
                ConfigValue = config.SC_ConfigValue,
                ConfigNote = config.SC_Note
            };
            return View(configModel);
        }

        [HttpPost]
        public ActionResult DataConfigEdit(DataConfigEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var config = systemConfigLogic.GetModelByGuidId(model.ConfigId.ToGuid());
            config.SC_ConfigKey = model.ConfigKey;
            config.SC_ConfigValue = model.ConfigValue;
            config.SC_Note = model.ConfigNote;
            bool updateResult = systemConfigLogic.Update(config) > 0;
            ViewBag.Msg = updateResult ? "修改成功" : "修改失败";
            return DataConfigEdit(model.ConfigId.ToGuid());
        }

        public ActionResult DataRouteEdit(Guid id)
        {
            var dataRoute = routeTableLogic.GetModelByGuidId(id);
            var routeModel = new DataRouteEditModel()
            {
                ConnString = dataRoute.RTC_ConnString,
                Database = dataRoute.RTC_DatabaseName,
                RouteID = dataRoute.RTC_ID.ToString(),
            };

            var gradeSelects = gradeinfoLogic.GetGrades().SingleOrDefault(u => u.Grade_ID == dataRoute.RTC_GradeID);

            var subjectSelects = subjectinfoLogic.GetSubjects().SingleOrDefault(u => u.Subject_ID == dataRoute.RTC_SubjectID);

            routeModel.GradeID = gradeSelects.Grade_Name;
            routeModel.SubjectID = subjectSelects.Subject_Name;
            return View(routeModel);
        }

        /// <summary>
        /// 数据库路由表修改
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DataRouteEdit(DataRouteEditModel route)
        {
            var dataRoute = routeTableLogic.GetModelByGuidId(route.RouteID.ToGuid());
            dataRoute.RTC_ConnString = route.ConnString;
            dataRoute.RTC_DatabaseName = route.Database;


            bool updateResult = routeTableLogic.Update(dataRoute) > 0;
            ViewBag.Msg = updateResult ? "修改成功" : "修改失败";

            return DataRouteEdit(route.RouteID.ToGuid());
        }

        [HttpPost]
        public JsonResult DataRouteDelete(Guid id)
        {
            var dataRoute = routeTableLogic.GetModelByGuidId(id);
            UpdateResponse rsp = new UpdateResponse();
            rsp.Success = routeTableLogic.Delete(dataRoute) > 0;
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        [AuthenUrlFilter]
        public ActionResult ClientVersionList()
        {
            //ClientVersionSearchModel
            return View();
        }

        [HttpPost]
        public ActionResult ClientVersionListResult(ClientVersionSearchModel search)
        {
            var pageResult = clientVersionLogic.ClientVersionSearch(search);
            return View(pageResult);
        }

        public ActionResult ClientVersionAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ClientVersionAdd(ClientVersionAddInfo model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            T_ClientVersionInfo clientVersion = new T_ClientVersionInfo()
            {
                Client_AccountID = Guid.Parse(account.AccountId),
                Client_AddDate = DateTime.Now,
                Client_ID = Guid.NewGuid(),
                Client_Note = model.ClientNote,
                Client_PublishDate = DateTime.Now,
                Client_State = 1,
                Client_Type = model.ClientType,
                Client_Version = model.ClientVersion,
                Client_DownloadUrl = "",
                Client_UpgradeID = model.UpgradeID,
                Client_FileName = model.FileName
            };

            bool updateResult = clientVersionLogic.Insert(clientVersion) > 0;
            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            if (updateResult)
            {
                return RedirectToAction("ClientVersionList");
            }
            return View();
        }

        /// <summary>
        /// 客户端升级信息删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ClientVersionDelete(Guid id)
        {
            var rsp = new UpdateResponse();
            var clientVersion = clientVersionLogic.GetModelByGuidId(id);
            clientVersion.Client_State = 0;
            rsp.Success = clientVersionLogic.Update(clientVersion) > 0;
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FriendLinkList()
        {

            return View();
        }

        [HttpPost]
        public ActionResult FriendLinkResult(int page = 1, int pageSize = 10)
        {
            FriendLinkLogic friendLinkLogic = new FriendLinkLogic();
            var pageResult = friendLinkLogic.FriendLinkResult(page, pageSize);
            return View(pageResult);
        }

        public ActionResult FriendLinkAdd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FriendLinkAdd(FriendLinkAddModel model)
        {
            T_FriendLink link = new T_FriendLink()
            {
                FL_AddDate = DateTime.Now,
                FL_ID = Guid.NewGuid(),
                FL_Link = model.LinkHref,
                FL_Index = model.LinkIndex,
                FL_Name = model.LinkName,
                FL_State = 1,
                FL_CreateId = Guid.Parse(account.AccountId),
                FL_Creator = account.AccountName
            };

            FriendLinkLogic friendLinkLogic = new FriendLinkLogic();
            bool updateResult = friendLinkLogic.Insert(link) > 0;

            ViewBag.Msg = updateResult ? "添加成功" : "添加失败";
            if (updateResult)
            {
                return RedirectToAction("FriendLinkList");
            }
            return View();
        }

        [HttpPost]
        public JsonResult FriendLinkDel(Guid id)
        {
            var rsp = new UpdateResponse();
            FriendLinkLogic friendLinkLogic = new FriendLinkLogic();
            var friendLink = friendLinkLogic.GetModelByGuidId(id);
            if (friendLink == null)
            {
                rsp.Success = false;
                rsp.Message = "删除失败";
            }
            else
            {
                rsp.Success = friendLinkLogic.Delete(friendLink) > 0;
                rsp.Message = rsp.Success ? "删除成功" : "删除失败";
            }
            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
    }
}
