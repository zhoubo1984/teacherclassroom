﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class GradeSubjectViewModel
    {
        public int id { set; get; }
        public int pId { set; get; }

        public string name { set; get; }

        public bool open { set; get; }

        public bool nocheck { set; get; }

        public string code { set; get; }

        //public bool checked
        //{
        //    get;
        //    set;
        //}
    }
}