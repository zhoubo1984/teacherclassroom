﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduPlatform.Manage.Models
{
    public class AccountUpdatePasswordModel
    {
        [Required]
        public string OldPassword { set; get; }

        [Required]
        public string NewPassword { set; get; }

        [Required]
        public string ConfirmPassword { set; get; }
    }
}