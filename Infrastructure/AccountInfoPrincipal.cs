﻿using EduPlatform.Logic.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace EduPlatform.Manage.Infrastructure
{
    public class AccountInfoPrincipal : IPrincipal
    {
        private AccountInfoIdentity identity;
        public AccountInfoPrincipal(string accountName)
        {
            AccountInfoLogic userLogic = new AccountInfoLogic();
            var account = userLogic.GetAccount(accountName);
            this.identity = new AccountInfoIdentity(account.Account_ID.ToString(), account.Account_LoginName, account.Account_IsAdmin);

        }

        public string AccountId { set; get; }
        public string AccountName { set; get; }
        public int IsAdmin { set; get; }

        public IIdentity Identity
        {
            get
            {
                return identity;
            }
        }

        public bool IsInRole(string role)
        {
            if (this.IsAdmin == 1 && role.ToLower() == "admin")
            { 
                return true; 
            }
            if (this.IsAdmin == 0 && role.ToLower() == "user")
            {
                return true;
            }
            return false;
        }
    }
}