﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeacherClassRoom.Web.Models
{
    public class TreeJsonModel
    {
        private List<TreeJsonModel> _nodes = new List<TreeJsonModel>();
        private List<string> _tags = new List<string>();

        public string id { get; set; }
        public string text { get; set; }
        public List<TreeJsonModel> nodes
        {
            get
            {
                return _nodes;
            }
            set
            {
                _nodes = value;
            }
        }
        public TreeJsonState state { get; set; }
        public List<string> tags 
        {
            get
            {
                return _tags;
            }
            set
            {
                _tags = value;
            }
        }
    }
}