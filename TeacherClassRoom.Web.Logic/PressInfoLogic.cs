﻿using EduPlatform.ServiceModel.Basal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeacherClassRoom.Sdk;

namespace TeacherClassRoom.Web.Logic
{
    public class PressInfoLogic
    {
        public BookPressResponse GetPressInfo(BookPressRequest request)
        {
            PressInfoSdk sdk = new PressInfoSdk();
            var rsp = sdk.GetPressInfo(request);

            return rsp;
        }
    }
}
